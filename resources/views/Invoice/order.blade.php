<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>Invoices</title> 
        <style type="text/css">         
        html,
        body {

           unicode-bidi: bidi-override !important;
           direction: unset !important;
           text-align:right;
            margin: 0;
            padding: 0;
            font-family: DejaVu Sans, sans-serif;
             font-size: 12pt;
            background-color: #eee;
        }

        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }

        .page {
            margin: 1cm auto;
            background: #fff;
            box-shadow: 0 4px 5px rgba(75, 75, 75, 0.2);
            outline: 0;
        }

        a {
            text-decoration: none;
            color: black;
        }

        /*table {*/
        /*    page-break-inside: avoid;*/
        /*}*/
        @page {
            orphans: 4;
            widows: 2;
        }

        @media print {
            html,
            body {
                background-color: #fff;
            }

            .page {
                width: initial !important;
                min-height: initial !important;
                margin: 0 !important;
                padding: 0 !important;
                border: initial !important;
                border-radius: initial !important;
                background: initial !important;
                box-shadow: initial !important;
            }
        }

        .page {
            width: 21cm;
            min-height: 29.7cm;

            padding-left: 2cm;
            padding-top: 2cm;
            padding-right: 2cm;
            padding-bottom: 2cm;
        }

        @page {
            size: A4 portrait;
            margin-left: 2cm;
            margin-top: 2cm;
            margin-right: 2cm;
            margin-bottom: 2cm;
        }

        .container {
            width: 100%;
            height: 257mm;
            position: relative;
            line-height: 1.6;
            font-size: 11pt;

        }

        footer {
            position: absolute;
            bottom: 0;
        }
    </style>
</head>
<body  >
@if(isset($orders))
@foreach($orders as $order)
<div  >
    <div >
        <div style=" background-color: #f0f0f0;margin:auto">
            <div style="padding:5px">
                <p style="text-align:center"> رقم الفاتوره: {{'#'.$order->id}}</p>
                <p style="text-align:center"> المتجر : كوفا </p>
                <p style="text-align:center"> وقت الطلب
                    : {{$order->created_at->timezone('AST')->format('dd/m/Y H:i:s ')}}</p>
                <p style="text-align:center"> حاله الطلب:{{$order->status}}</p>
            </div>
        </div>
        <div>

            <center style="font-size:15pt"> معلومات الدفع و الشحن</center>
            {{--        <table style="direction:rtl;magin:auto;float:left" width="90%" border="0">--}}
            {{--           <tr>--}}
            {{--               <td>الاسم </td>--}}
            {{--               <td>الجوال </td>--}}
            {{--               <td>الايميل </td>--}}
            {{--           </tr>--}}
            {{--            <tr>--}}
            {{--                <td>{{$user->name}} </td>--}}
            {{--                <td>{{$user->mobile_number}} </td>--}}
            {{--                <td>{{$user->email}} </td>--}}
            {{--            </tr>--}}
            {{--        </table>--}}
            {{--        <p>العميل </p>--}}
            <p style="text-align:right;direction: rtl"> عنوان الشحن
                : {{$order->address->city. " ". $order->address->area}}</p>
            <p style="text-align:right"> طريقه الشحن : كوفا </p>
            <p style="text-align:right"> طريقه الدفع
                : {{ $order->transaction->payment_method == 'CASH' ? 'الدفع عند التوصيل ': 'الدفع عن طريق الانترنت' }}</p>

            <hr>

            <p style="text-align:center;font-size:15pt"> تفاصيل المنتجات </p>

            <table style="float:left" width="100%" border="1">
                <tr>
                    <td>المنتجات</td>
                    <td> خيارات المنتجات</td>
                    <td> سعر المنتج</td>
                    <td> الكميه</td>
                    <td> السعر الاجمالي</td>
                </tr>
                @foreach($order->cart->items as $item)
                <tr>
                    <td> {{$item->buy->name }}</td>
                    <td></td>
                    <td>{{(int) $item->buy->price   }} SAR</td>
                    <td>{{ $item->quantity  }} </td>
                    <td>{{ $item->buy->price * $item->quantity  }} SAR</td>
                </tr>
                @endforeach

            </table>

            <hr>
            <p style="text-align:center; font-size:15pt"> تفاصيل الاسعار</p>
            <p style="text-align:right"> قيمه المنتجات : {{$order->total}} </p>
            <p style="text-align:right"> قيمه التوصيل : {{$order->shipments->first()->price }}  </p>
            <p style="text-align:right"> الدفع عند التوصيل
                : {{$order->transaction->payment_method == "CASH" ? $order->shipments->first()->price + $order->total : 0}} </p>
            <p style="text-align:right"> المجموع الكلي : {{ $order->shipments->first()->price + $order->total }} </p>


            <div style=" background-color: #f0f0f0;margin:auto">
                <div style="padding:5px">
                    <p style="text-align:center"> شكرا لشرائك من كوفا نتمنى انها كانت رحله تسوق سعيده </p>
                    <p style="text-align:center"> الطلبات السابقه </p>
                    <p style="text-align:center"> جميع الحقوق محفوظه كوفا @ 2020 </p>
                </div>
            </div>
        </div>
    </div>

</div>
@endforeach
@else
<div class="page" contenteditable="true">
    <div class="container">
        <div style=" background-color: #f0f0f0;margin:auto">
            <div style="padding:5px">
                <p style="text-align:center"> رقم الفاتوره: {{'#'.$order->id}}</p>
                <p style="text-align:center"> المتجر : كوفا </p>
                <p style="text-align:center"> وقت الطلب
                    : {{$order->created_at->timezone('AST')->format('dd/m/Y H:i:s ')}}</p>
                <p style="text-align:center"> حاله الطلب:{{$order->status}}</p>
            </div>
        </div>
        <div>

            <center style="font-size:15pt"> معلومات الدفع و الشحن</center>
            {{--        <table style="direction:rtl;magin:auto;float:left" width="90%" border="0">--}}
            {{--           <tr>--}}
            {{--               <td>الاسم </td>--}}
            {{--               <td>الجوال </td>--}}
            {{--               <td>الايميل </td>--}}
            {{--           </tr>--}}
            {{--            <tr>--}}
            {{--                <td>{{$user->name}} </td>--}}
            {{--                <td>{{$user->mobile_number}} </td>--}}
            {{--                <td>{{$user->email}} </td>--}}
            {{--            </tr>--}}
            {{--        </table>--}}
            {{--        <p>العميل </p>--}}
            <p style="text-align:right;direction: rtl"> عنوان الشحن
                : {{$order->address->city. " ". $order->address->area}}</p>
            <p style="text-align:right"> طريقه الشحن : كوفا </p>
            <p style="text-align:right"> طريقه الدفع
                : {{ $order->transaction->payment_method == 'CASH' ? 'الدفع عند التوصيل ': 'الدفع عن طريق الانترنت' }}</p>

            <hr>

            <p style="text-align:center;font-size:15pt"> تفاصيل المنتجات </p>

            <table direction="rtl" style="direction:rtl;float:left" width="100%" border="1">
                <tr>
                    <td>المنتجات</td>
                    <td> خيارات المنتجات</td>
                    <td> سعر المنتج</td>
                    <td> الكميه</td>
                    <td> السعر الاجمالي</td>
                </tr>
                @foreach($order->cart->items as $item)
                <tr>
                    <td> {{$item->buy->name }}</td>
                    <td></td>
                    <td>{{(int) $item->buy->price   }} SAR</td>
                    <td>{{ $item->quantity  }} </td>
                    <td>{{ $item->buy->price * $item->quantity  }} SAR</td>
                </tr>
                @endforeach

            </table>

            <hr>
            <p style="text-align:center; font-size:15pt"> تفاصيل الاسعار</p>
            <p style="text-align:right"> قيمه المنتجات : {{$order->total}} </p>
            <p style="text-align:right"> قيمه التوصيل : {{$order->shipments->first()->price }}  </p>
            <p style="text-align:right"> الدفع عند التوصيل
                : {{$order->transaction->payment_method == "CASH" ? $order->shipments->first()->price + $order->total : 0}} </p>
            <p style="text-align:right"> المجموع الكلي : {{ $order->shipments->first()->price + $order->total }} </p>


            <div style=" background-color: #f0f0f0;margin:auto">
                <div style="padding:5px">
                    <p style="text-align:center"> شكرا لشرائك من كوفا نتمنى انها كانت رحله تسوق سعيده </p>
                    <p style="text-align:center"> الطلبات السابقه </p>
                    <p style="text-align:center"> جميع الحقوق محفوظه كوفا @ 2020 </p>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    window.onload = function () {
        setTimeout(function () {
            print();
        }, 1500)
    };
</script>
@endif

</body>
</html>
