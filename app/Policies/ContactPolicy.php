<?php

namespace App\Policies;

use App\Domains\ContactUs\Models\ContactUS;
use App\Domains\Rating\Models\Rating;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContactPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param ContactUS $contact
     * @return false
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * @param User $user
     * @param ContactUS $contact
     * @return false
     */
    public function update(User $user,   ContactUS $contact)
    {
        return false;
    }

    /**
     * @param User $user
     * @param ContactUS $contact
     * @return false
     */
    public function delete(User $user,  ContactUS $contact)
    {
        return false;
    }

    public function viewAny(User $user)
    {
        return $user->hasRole('super-admin') || $user->hasPermissionTo('view-contact');
    }
}
