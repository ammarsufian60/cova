<?php

namespace App\Policies;

use App\Domains\Cart\Models\CartItem;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CartItemPolicy
{
    use HandlesAuthorization;

    /**
     * Check to blanket-add all permissions.
     *
     * @param User|null $user
     * @param $ability
     *
     * @return bool
     */
    public function before(User $user, $ability)
    {
//        if ($user->hasRole('super-admin')) {
//            return true;
//        }
    }

    /**
     * Determine whether the user can view the cart item.
     *
     * @param \App\User                         $user
     * @param \App\Domains\Cart\Models\CartItem $cartItem
     *
     * @return mixed
     */
    public function view(User $user, CartItem $cartItem)
    {
        return $user->hasRole('super-admin') || $user->hasPermissionTo('view-cart-item');
    }

    /**
     * Determine whether the user can create cart items.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
        //return $user->hasPermissionTo('create-cart-item');
    }

    /**
     * Determine whether the user can update the cart item.
     *
     * @param \App\User                         $user
     * @param \App\Domains\Cart\Models\CartItem $cartItem
     *
     * @return mixed
     */
    public function update(User $user, CartItem $cartItem)
    {
        return false ;
        //return $user->hasPermissionTo('update-cart-item');
    }

    /**
     * Determine whether the user can delete the cart item.
     *
     * @param \App\User                         $user
     * @param \App\Domains\Cart\Models\CartItem $cartItem
     *
     * @return mixed
     */
    public function delete(User $user, CartItem $cartItem)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the cart item.
     *
     * @param \App\User                         $user
     * @param \App\Domains\Cart\Models\CartItem $cartItem
     *
     * @return mixed
     */
    public function restore(User $user, CartItem $cartItem)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the cart item.
     *
     * @param \App\User                         $user
     * @param \App\Domains\Cart\Models\CartItem $cartItem
     *
     * @return mixed
     */
    public function forceDelete(User $user, CartItem $cartItem)
    {
        return false;
    }

    /**
     * Check whether the user can view the resource on Nova.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('super-admin') || $user->hasPermissionTo('view-cart-item');
    }
}
