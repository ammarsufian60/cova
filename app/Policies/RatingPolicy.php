<?php

namespace App\Policies;

use App\Domains\Rating\Models\Rating;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RatingPolicy
{
    use HandlesAuthorization;
    /**
     * Check to blanket-add all permissions.
     *
     * @param User|null $user
     * @param $ability
     *
     * @return bool
     */
    public function before(User $user, $ability)
    {
//        if ($user->hasRole('super-admin')) {
//            return true;
//        }
    }

    /**
     * @param User $user
     * @param Rating $rating
     * @return bool
     */
    public function view(User $user, Rating $rating)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('view-rating');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
	return false;
//        return $user->hasRole('super-admin');
//        return $user->hasPermissionTo('create-order');
    }

    /**
     * @param User $user
     * @param Rating $rating
     * @return bool
     */
    public function update(User $user, Rating $rating)
    {
        return  $user->hasRole('super-admin');
//        return $user->hasPermissionTo('update-order');
    }

    /**
     * @param User $user
     * @param Rating $rating
     * @return false
     */
    public function delete(User $user, Rating $rating)
    {
        return false;
        //return $user->hasPermissionTo('delete-order');
    }

    /**
     * @param User $user
     * @param Rating $rating
     * @return false
     */
    public function restore(User $user, Rating $rating)
    {
        return false;
    }

    /**
     * @param User $user
     * @param Rating $rating
     * @return false
     */
    public function forceDelete(User $user, Rating $rating)
    {
        return false;
    }

    /**
     * Check whether the user can view the resource on Nova.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('super-admin');
    }

}
