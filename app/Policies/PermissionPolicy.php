<?php

namespace App\Policies;

use App\Domains\User\Models\Permission;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;
    /**
     * Check to blanket-add all permissions.
     *
     * @param User|null $user
     * @param $ability
     *
     * @return bool
     */
    public function before(User $user, $ability)
    {
//        if ($user->hasRole('super-admin')) {
//            return true;
//        }
    }

    /**
     * @param User $user
     * @param Permission $permission
     * @return bool
     */
    public function view(User $user, Permission $permission)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('view-permission');
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasRole('super-admin');
//        return $user->hasPermissionTo('create-order');
    }

    /**
     * @param User $user
     * @param Permission $permission
     * @return bool
     */
    public function update(User $user, Permission $permission)
    {
        return  $user->hasRole('super-admin');
//        return $user->hasPermissionTo('update-order');
    }

    /**
     * @param User $user
     * @param Permission $permission
     * @return false
     */
    public function delete(User $user, Permission $permission)
    {
        return false;
        //return $user->hasPermissionTo('delete-order');
    }

    /**
     * @param User $user
     * @param Permission $permission
     * @return false
     */
    public function restore(User $user, Permission $permission)
    {
        return false;
    }

    /**
     * @param User $user
     * @param Permission $permission
     * @return false
     */
    public function forceDelete(User $user, Permission $permission)
    {
        return false;
    }

    /**
     * Check whether the user can view the resource on Nova.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasRole('super-admin');
    }

}
