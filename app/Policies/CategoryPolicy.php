<?php

namespace App\Policies;

use App\Domains\Category\Models\Category;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Check to blanket-add all permissions.
     *
     * @param User|null $user
     * @param $ability
     *
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the category.
     *
     * @param \App\User                             $user
     * @param \App\Domains\Category\Models\Category $category
     *
     * @return mixed
     */
    public function view(User $user, Category $category)
    {
        return $user->hasPermissionTo('view-category');
    }

    /**
     * Determine whether the user can create categories.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create-category');
    }

    /**
     * Determine whether the user can update the category.
     *
     * @param \App\User                             $user
     * @param \App\Domains\Category\Models\Category $category
     *
     * @return mixed
     */
    public function update(User $user, Category $category)
    {
        return $user->hasPermissionTo('update-category');
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param \App\User                             $user
     * @param \App\Domains\Category\Models\Category $category
     *
     * @return mixed
     */
    public function delete(User $user, Category $category)
    {
        return $user->hasPermissionTo('delete-category');
    }

    /**
     * Determine whether the user can restore the category.
     *
     * @param \App\User                             $user
     * @param \App\Domains\Category\Models\Category $category
     *
     * @return mixed
     */
    public function restore(User $user, Category $category)
    {
        return false;
        //return $user->hasPermissionTo('restore-category');
    }

    /**
     * Determine whether the user can permanently delete the category.
     *
     * @param \App\User                             $user
     * @param \App\Domains\Category\Models\Category $category
     *
     * @return mixed
     */
    public function forceDelete(User $user, Category $category)
    {
        return false;
//        return $user->hasPermissionTo('force-delete-category');
    }

    /**
     * Check whether the user can view the resource on Nova.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('view-category');
    }

}
