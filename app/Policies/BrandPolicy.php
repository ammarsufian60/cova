<?php

namespace App\Policies;

use App\Domains\Brand\Models\Brand;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BrandPolicy
{
    use HandlesAuthorization;

    /**
     * Check to blanket-add all permissions.
     *
     * @param User|null $user
     * @param $ability
     *
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->hasRole('super-admin')) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the brand.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Brand\Models\Brand $brand
     *
     * @return mixed
     */
    public function view(User $user, Brand $brand)
    {
        return $user->hasPermissionTo('view-brand');
    }

    /**
     * Determine whether the user can create brands.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create-brand');
    }

    /**
     * Determine whether the user can update the brand.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Brand\Models\Brand $brand
     *
     * @return mixed
     */
    public function update(User $user, Brand $brand)
    {
        return $user->hasPermissionTo('update-brand');
    }

    /**
     * Determine whether the user can delete the brand.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Brand\Models\Brand $brand
     *
     * @return mixed
     */
    public function delete(User $user, Brand $brand)
    {
        return false;
       // return $user->hasPermissionTo('delete-brand');
    }

    /**
     * Determine whether the user can restore the brand.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Brand\Models\Brand $brand
     *
     * @return mixed
     */
    public function restore(User $user, Brand $brand)
    {
        return false;
        //return $user->hasPermissionTo('restore-brand');
    }

    /**
     * Determine whether the user can permanently delete the brand.
     *
     * @param \App\User                       $user
     * @param \App\Domains\Brand\Models\Brand $brand
     *
     * @return mixed
     */
    public function forceDelete(User $user, Brand $brand)
    {
        return false;
    }

    /**
     * Check whether the user can view the resource on Nova.
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('view-brand');
    }
}
