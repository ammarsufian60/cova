<?php

namespace App\Policies;

use App\Domains\User\Models\Driver;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DriverPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param Driver $driver
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('create-driver');
    }

    /**
     * @param User $user
     * @param Driver $driver
     * @return bool
     */
    public function update(User $user,  Driver $driver)
    {
        return $user->hasRole('super-admin') ||  $user->hasPermissionTo('update-driver');
    }

    /**
     * @param User $user
     * @param Driver $driver
     * @return false
     */
    public function delete(User $user, Driver $driver)
    {
        return false;
    }

     /**
     * @param User $user
     * @param Product $product
     * @return false
     */
    public function view(User $user)
    {
        return  $user->hasRole('super-admin') ||  $user->hasRole('driver-management');
    }

     /**
     * @param User $user
     * @param Product $product
     * @return false
     */
    public function viewAny(User $user)
    {
        return  $user->hasRole('super-admin') ||  $user->hasRole('driver-management');
    }

}
