<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionPolicy
{
    use HandlesAuthorization;
    /**
     * Determine whether the user can view the product.
     *
     * @param \App\User                           $user
     * @param \App\Domains\Product\Models\Product $product
     *
     * @return mixed
     */
    public function before(User $user, $ability)
    {
//        if ($user->hasRole('super-admin')) {
//            return true;
//        }
    }

    public function view(User $user)
    {
        return $user->hasRole('super-admin') || $user->hasPermissionTo('view-transaction');
    }

    public function viewAny(User $user)
    {
        return $user->hasRole('super-admin') || $user->hasPermissionTo('view-transaction');
    }
}
