<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Domains\Order\Http\Resources\ProviderOrderHistory

class PushPendingOrder implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * PushPendingOrder constructor.
     * @param $order
     * @param $branch_id
     */
     public $order;
     public $branch_id;

    public function __construct($order,$branch_id)
    {
        $this->order =new ProviderOrderHistory($order);
        $this->branch_id = $branch_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['PendingOrder'.$this->branch_id];
    }


    public function broadcastAs()
    {
         return 'PendingOrder'.$this->branch_id;
    }

}
