<?php

namespace App\Console\Commands;

use App\Domains\Order\Models\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateStatusOfPendingOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pending:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update status of pending order when created before 4 Hours and still pending';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders =Order::whereIn('status',['pending','waiting-for-acceptance'])->where('created_at','<',Carbon::now()->subHour(4))->get();

        collect($orders)->each(function($order){
            $order->update(['status' => 'canceled']);
        });

    }
}
