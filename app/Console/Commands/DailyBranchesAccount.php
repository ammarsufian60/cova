<?php

namespace App\Console\Commands;

use App\Domains\Account\Models\Account;
use App\Domains\Branch\Models\Branch;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DailyBranchesAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accounts:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate the Total of branches fees and print invoices Command ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->delivery_orders_fees=0;
        $this->credit_card_order_fees =0;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //get branches where has local brands

        $branches = Branch::whereHas('brand',function($query){
            $query->where('is_international',0);
        })->get();
        
        collect($branches)->each(function($branch){
         
        $orders = $branch->orders->where('created_at','>',Carbon::now()->firstOfMonth()); //->where('created_at','>',Carbon::today());

            if(!is_null($orders))
            {
                $this->order_total = 0;
                $this->delivery_orders_fees =0;
                $this->credit_card_order_fees =0;

               //calculate the delivery order fees and credit card fees
                collect($orders)->each(function($order)
                {
  		    $this->order_total += $order->transaction->amount;
                    $this->delivery_orders_fees += !is_null($order->shipments) ? $order->shipments->first()->price : 0;
                    $this->credit_card_order_fees += $order->transaction->payment_method== 'ONLINE' ? $order->transaction->amount * 0.025 : 0  ;
                });
 
                 //check the subscription type and create the account records
               if($branch->subscription_type =='percentage-on-sales'){
                   Account::create([
                       'branch_id' => $branch->id,
                       'order_total' => $this->order_total,
                       'delivery_orders_fees' => $this->delivery_orders_fees,
                       'credit_card_order_fees'=> $this->credit_card_order_fees,
		       'total_fees' => $this->delivery_orders_fees +$this->credit_card_order_fees,
                       'payment_total' => ($this->order_total * ((int)$branch->subscription_amount/100)) + $this->delivery_orders_fees +  $this->credit_card_order_fees,
                   ]);
               }else{
                   Account::create([
                       'branch_id' => $branch->id,
                       'order_total' => $this->order_total,
                       'delivery_orders_fees' => $this->delivery_orders_fees,
                       'credit_card_order_fees'=> $this->credit_card_order_fees,
                       'total_fees' => $this->delivery_orders_fees +$this->credit_card_order_fees,
         	       'payment_total' => $branch->subscription_amount + $this->delivery_orders_fees +  $this->credit_card_order_fees,
                   ]);
               }

            }
        });
    }
}
