<?php

namespace App\Domains\Account\Models;

use App\Domains\Branch\Models\Branch;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'branch_id',
        'order_total',
        'delivery_orders_fees',
        'credit_card_order_fees',
        'total_fees',
        'payment_total',
        'is_paid'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
}
