<?php


namespace App\Domains\Account\Actions;


use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;

class ExportAccounts extends DownloadExcel implements WithMapping
{

    /**
     * @param User $user
     *
     * @return array
     */
    public function map($account): array
    {
        return [
            $account->branch->name,
            $account->order_total,
            $account->delivery_orders_fees,
            $account->credit_card_order_fees,
            $account->total_fees,
            $account->payment_total,
            $account->is_paid ==0 ? false : true,
        ];
    }


}
