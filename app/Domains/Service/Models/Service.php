<?php

namespace App\Domains\Service\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'service_name',
        'value',
        'parent_service_id'
    ];

    protected $with = ['sibling'];

    public function sibling()
    {
        return $this->belongsTo(Service::class ,  'parent_service_id');
    }
}
