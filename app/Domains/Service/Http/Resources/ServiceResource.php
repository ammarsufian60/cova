<?php


namespace App\Domains\Service\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;
use App\domains\Service\Models\Service;

class ServiceResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
      return [
          'service_name' => isset($this->service_name)? $this->service_name: null,
          'title' => isset($this->title) ? $this->title:null,
          'value' => isset($this->value)?$this->value:null,
//          'siblings' => Service::where('parent_service_id',$this->id)->get()
      ];
    }

}
