<?php


namespace App\Domains\Service\Http\Controllers;


use App\Domains\Service\Http\Requests\SerivceRequest;
use App\Domains\Service\Http\Resources\ServiceResource;
use App\Domains\Service\Models\Service;

class ServiceController
{

    public function __invoke(SerivceRequest $request)
    {
        try{

            $service = Service::where('service_name',$request->service_name)->first();
            if(is_null($service)){
             return  new ServiceResource($service);
            }
           
            $child_services =Service::where('parent_service_id', $service->id)->get();
 
           if(!is_null($child_services))
           {
            $child_services->push($service);

//	    return response()->json($child_services);
            return ServiceResource::collection($child_services);
          
          }
          else{
	   return  new ServiceResource($service);
          }

        }catch(\Exception $e){
            return
            response()->json(
                ['message' => $e->getMessage() ]  //'something wrong please try again ..']
                ,500 );
        }

    }
}
