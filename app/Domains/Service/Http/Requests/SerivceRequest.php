<?php


namespace App\Domains\Service\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class SerivceRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return \string[][]
     */
    public function rules()
    {
        return [
          'service_name' => ['required','string'],
        ];
    }
}
