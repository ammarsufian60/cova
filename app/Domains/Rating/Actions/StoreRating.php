<?php


namespace App\Domains\Rating\Actions;


use App\Domains\Order\Models\Order;
use App\Domains\Rating\Models\Rating;
use Illuminate\Support\Facades\Auth;

class StoreRating
{

    protected $params;

    /**
     * StoreRating constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
       $this->params = $params;
    }

    public function handle()
    {
       return Rating::create([
            'branch_id' => $this->params['branch_id'],
            'user_id' => Auth::user()->id,
            'order_id'=> $this->params['order_id'],
            'rate' => $this->params['rate'],
            'driver_rating' => isset($this->params['driver_rating'])?$this->params['driver_rating']: null,
            'branch_rating' => isset($this->params['branch_rating'])?$this->params['branch_rating'] :null,
            'comment' => isset($this->params['comment'])? $this->params['comment'] : null
        ]);
    }
}
