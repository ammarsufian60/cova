<?php


namespace App\Domains\Rating\Actions;


use App\Domains\Rating\Models\Rating;
use Illuminate\Support\Facades\Auth;

class DeleteRating
{
    protected $branch_id;

    /**
     * DeleteRating constructor.
     * @param $branch_id
     */
    public function __construct($branch_id)
    {
        $this->branch_id = $branch_id;
    }

    public function handle()
    {
        $rating = Rating::where('branch_id',$this->branch_id)->where('user_id',Auth::user()->id)->first();
        
        if(!is_null($rating)){
         $rating->delete();
        }

        return 'success';
    }
}
