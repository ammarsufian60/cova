<?php


namespace App\Domains\Rating\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class StoreRatingRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
     	    'rate' => ['required'],
             'order_id' =>['required','unique:ratings,order_id'],
	    'branch_id' =>['required','exists:branches,id']
        ];
    }
}
