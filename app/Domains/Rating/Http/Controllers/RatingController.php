<?php


namespace App\Domains\Rating\Http\Controllers;

use App\Domains\Rating\Actions\DeleteRating;
use App\Domains\Rating\Actions\StoreRating;
use App\Domains\Rating\Http\Requests\StoreRatingRequest;
use App\Facades\Rating;
use App\Http\Controllers\Controller;

class RatingController extends Controller
{

    public function store(StoreRatingRequest  $request)
    {
        try{
             $rating = Rating::store($request->all());
        }catch(\Exception $e)
        {
            return response()->json(['message' => $e->getMessage()],500);
        }
        return response()->json(['message' => 'success']);
    }

    public function delete(StoreRatingRequest  $request)
    {
        try{
            $rating = (new DeleteRating($request->branch_id))->handle();

        }catch(\Exception $e)
        {
            return response()->json(['message' => $e->getMessage()],500);
        }
        return response()->json(['message' => 'success']);
    }

}
