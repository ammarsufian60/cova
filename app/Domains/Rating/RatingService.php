<?php


namespace App\Domains\Rating;


use App\Domains\Rating\Actions\DeleteRating;
use App\Domains\Rating\Actions\StoreRating;

class RatingService
{
    /**
     * @param array $params
     * @return mixed
     */
    public function store(array $params)
    {
        return (new StoreRating($params))->handle();
    }

    /**
     * @param $branch_id
     * @return string
     */
    public function delete($branch_id)
    {
        return (new DeleteRating($branch_id))->handle();
    }
}
