<?php


namespace App\Domains\Transaction\Actions;


use App\Domains\Transaction\Models\Transaction;

class CreateTransaction
{

    protected $order_id;
    protected $amount;
    protected $payment_method;
    protected $payment_id;
    /**
     * CreateTransaction constructor.
     * @param $order_id
     * @param $payment_method
     * @param $amount
     */
    public function __construct($order_id,$payment_method,$amount,$payment_id = null)
    {
        $this->order_id = $order_id;
        $this->payment_method = $payment_method;
        $this->amount =$amount;
        $this->payment_id= $payment_id;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $transaction =Transaction::create([
            'order_id' => $this->order_id,
            'payment_method' => $this->payment_method,
            'amount' => $this->amount,
            'status' => 'pending',
            'payment_id' => $this->payment_id,
        ]);

        return $transaction;
    }
}
