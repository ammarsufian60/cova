<?php


namespace App\Domains\Transaction;


use App\Domains\Transaction\Actions\CreateTransaction;

class TransactionService
{
    /**
     * @param $order_id
     * @param $payment_method
     * @param $amount
     * @return mixed
     */
    public function create($order_id ,$payment_method ,$amount )
    {
        return (new CreateTransaction($order_id,$payment_method,$amount))->handle();
    }
}
