<?php

namespace App\Domains\Transaction\Models;

use App\Domains\Order\Models\Order;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable =[
        'payment_method',
        'amount',
        'order_id',
        'status',
        'payment_id',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
