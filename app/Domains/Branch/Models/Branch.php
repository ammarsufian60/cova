<?php

namespace App\Domains\Branch\Models;

use App\Domains\Brand\Models\Brand;
use App\Domains\Category\Models\Category;
use App\Domains\Order\Models\Order;
use App\Domains\Rating\Models\Rating;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\File;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Branch extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable= [
        'name',
        'latitude',
        'longitude',
        'brand_id',
        'open_hour',
        'close_hour',
        'status',
        'estimate_delivery_time',
        'is_discounted',
        'discount_rate',
        'subscription_amount',
        'subscription_type',
        'contract_expiry_date',
        'manager_id',
    ];

    protected $casts =[
     'contract_expiry_date' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class,'branch_categories','branch_id','category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rating()
    {
        return $this->hasMany(Rating::class);
    }
    public function registerMediaCollections()
    {
        $this->addMediaCollection('branch')
            ->acceptsFile(function (File $file) {
                return in_array($file->mimeType, [
                    'image/jpeg',
                    'image/png',
                ]);
            })
            ->singleFile();
    }

    /**
     * @return string|null
     */
    public function getImageAttribute()
    {
        $image = $this->getFirstMediaUrl('branch');
        return empty($image)? null :config('app.url') . $image;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(User::class,'manager_id')->where('role','Manager');
    }
}
