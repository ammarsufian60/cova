<?php


namespace App\Domains\Branch;


use App\Domains\Branch\Actions\GetBranchByProviderId;
use App\Domains\Branch\Actions\NearerBranches;

class BranchServices
{
    /**
     * @param $latitude
     * @param $longitude
     * @param int $discounted
     * @param null $distance
     * @return mixed
     */
    public function nearby($latitude, $longitude,$discounted =0, $zone=7)
    {
        return (new NearerBranches($latitude,$longitude,$discounted,$zone))->handle();
    }

    /**
     * @param $provider_id
     * @return mixed
     */
    public function getBranchByProviderId($provider_id)
    {
        return (new GetBranchByProviderId($provider_id))->handle();
    }

}
