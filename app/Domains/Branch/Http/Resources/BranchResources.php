<?php


namespace App\Domains\Branch\Http\Resources;


use App\Domains\Brand\Http\Resources\BrandResources;
use App\Domains\Category\Http\Resources\CategoryResources;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchResources extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
       return [
           'id'=> $this->id,
           'name' =>$this->name,
           'latitude' => $this->latitude,
           'longitude' => $this->longitude,
           'open_hour' => $this->open_hour,
           'close_hour' => $this->close_hour,
           'image' => $this->image,
           'rating' => $this->ratings->count(),
           'delivery_time'=> $this->estimate_delivery_time ,
           'brand' => new BrandResources($this->brand),
           'categories' => CategoryResources::collection($this->categories),
           'is_discounted' => $this->is_discounted,
           'discount_rate' => $this->discount_rate
       ];
    }
}
