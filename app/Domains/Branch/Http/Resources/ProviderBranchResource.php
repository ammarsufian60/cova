<?php


namespace App\Domains\Branch\Http\Resources;


use App\Domains\Brand\Http\Resources\BrandResources;
use App\Domains\Category\Http\Resources\CategoryResources;
use App\Domains\User\Http\Resources\DriverShipmentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProviderBranchResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'orders_number' => $this->orders->count(),
            'total_profit'  => $this->orders->sum('total'),
        ];
    }
}
