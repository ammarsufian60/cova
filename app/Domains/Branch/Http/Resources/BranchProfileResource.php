<?php


namespace App\Domains\Branch\Http\Resources;


use App\Domains\Brand\Http\Resources\BrandResources;
use App\Domains\Brand\Http\Resources\ProviderBrandResources;
use App\Domains\Category\Http\Resources\CategoryResources;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchProfileResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'name' =>$this->name,
            'open_hour' => $this->open_hour,
            'close_hour' => $this->close_hour,
            'image' => $this->image,
            'rating' => $this->ratings->count(),
            'delivery_time'=> $this->estimate_delivery_time ,
            'is_discounted' => $this->is_discounted,
            'discount_rate' => $this->discount_rate,
            'brand' => new ProviderBrandResources($this->brand),
            'categories' => CategoryResources::collection($this->categories),
        ];
    }
}
