<?php


namespace App\Domains\Branch\Http\Controllers;

use App\Domains\Order\Models\Order;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MapsController extends Controller
{

    public function __invoke(Request $request , $order_id)
    {
        $address = Order::find($order_id)->address;

        $latitude = $address->latitude;
        $longitude =  $address->longitude;

        return view('Maps.map',compact('latitude','longitude'));

    }
}
