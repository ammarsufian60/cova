<?php


namespace App\Domains\Branch\Http\Controllers;


use App\Domains\Branch\Actions\GetBranchByProviderId;
use App\Domains\Branch\Http\Resources\BranchResources;
use App\Domains\Branch\Http\Resources\ProviderBranchResource;
use App\Facades\BranchFacade;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BranchController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function nearby(Request $request)
    {
       $this->validate($request, [
        'latitude' =>['required' ],
        'longitude' => ['required'],
      ]);

      
       $branches = BranchFacade::nearby($request->latitude , $request->longitude);

      return BranchResources::collection($branches);
    }

    /**
     * @param Request $request
     */
    public function discounted(Request $request)
    {
        $this->validate($request, [
            'latitude' =>['required' ,'string'],
            'longitude' => ['required','string'],
        ]);

        $branches = BranchFacade::nearby($request->latitude , $request->longitude,1);

        return BranchResources::collection($branches);

    }

    public function getBranchByManager(Request $request)
    {
        try {
          $branch = BranchFacade::GetBranchByProviderId(Auth::user()->id);

        }catch (\Exception $e)
        {
            return response()->json(['message' => $e->getMessage()],500);
        }
        return new ProviderBranchResource($branch);
    }

}
