<?php


namespace App\Domains\Branch\Actions;


use App\Domains\Branch\Models\Branch;

class NearerBranches
{
    protected $latitude;
    protected $longitude;
    protected $discounted;

    /**
     * NearerBranches constructor.
     * @param $latitude
     * @param $longitude
     * @param int $discounted
     * @param int $distance
     */
    public function __construct($latitude , $longitude,$discounted =0 ,$zone= 5)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->discounted = $discounted;
        $this->zone = $zone;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $locations = $this->discounted ==0 ?
                     Branch::where('status',true)->get() :
                     Branch::where('status',true)->where('is_discounted',1)->get();

        $locations = $locations->reject(function (Branch $branch) {
             $this->distance =haversign($this->latitude,$this->longitude,$branch->latitude,$branch->longitude) ;
             $branch->distance =$this->distance;
             return  $this->distance >  $this->zone  ;

        });

        $locations = collect($locations)->sortBy('distance')->unique('brand_id');

       return $locations ;
    }
}
