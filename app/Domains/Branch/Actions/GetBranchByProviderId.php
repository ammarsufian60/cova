<?php


namespace App\Domains\Branch\Actions;


use App\Domains\Branch\Models\Branch;

class GetBranchByProviderId
{
    /**
     * GetBranchByProviderId constructor.
     * @param $provider_id
     */
    public function __construct($provider_id)
    {
        $this->provider_id =$provider_id;
    }

    /**
     * @return mixed
     */

    public function handle()
    {
       return Branch::where('manager_id',$this->provider_id)->first();
    }
}
