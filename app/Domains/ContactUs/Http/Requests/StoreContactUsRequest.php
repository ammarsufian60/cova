<?php


namespace App\Domains\ContactUs\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class StoreContactUsRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    /**
     * @return \string[][]
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'mobile_number' => ['required'],
            'message' => ['required', 'string'],
        ];
    }
}
