<?php

namespace App\Domains\ContactUs\Http\Controllers;

use App\Domains\ContactUs\Http\Requests\StoreContactUsRequest;
use App\Domains\ContactUs\Models\ContactUS;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{

    public function store(StoreContactUsRequest  $request)
    {
        ContactUS::create([
           'message' => $request->message,
           'user_name' => $request->name,
           'mobile_number' => $request->mobile_number,
           'user_id' =>isset(Auth::user()->id)?Auth::user()->id: 1,
        ]);

        return response()->json(['message' => 'success']);
    }
}
