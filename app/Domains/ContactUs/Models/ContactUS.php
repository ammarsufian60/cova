<?php

namespace App\Domains\ContactUs\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ContactUS extends Model
{
    protected $fillable =[
        'user_name',
        'user_id',
        'message',
        'mobile_number'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
