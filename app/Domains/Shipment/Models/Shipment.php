<?php

namespace App\Domains\Shipment\Models;

use App\Domains\Order\Models\Order;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    protected $fillable =[
        'order_id',
        'status',
        'driver_id',
        'price',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver()
    {
        return $this->belongsTo(User::class,'driver_id');
    }
}
