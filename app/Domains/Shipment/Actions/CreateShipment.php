<?php


namespace App\Domains\Shipment\Actions;


use App\Domains\Branch\Models\Branch;
use App\Domains\Order\Models\Order;
use App\Domains\Shipment\Models\Shipment;
use App\Domains\Service\Models\Service;

class CreateShipment
{
    protected $order_id;

    /**
     * CreateShipment constructor.
     * @param $order_id
     * @param $branch_id
     * @param null $price
     *
     */
    public function __construct($order_id,$branch_id , $price=null)
    {
        $this->order_id = $order_id;
        $this->branch = Branch::find($branch_id);
        $this->price =$price;
        $this->km_price = Service::where('service_name','delivery_km_price_SR')->first()->value;
        $this->base_delivery_price = Service::where('service_name','base_delivery_price')->first()->value;
    }

    public function handle()
    {
        $order= Order::find($this->order_id);

        Shipment::create([
            'order_id' => $this->order_id ,
            'status' => 'pending' ,
            'price' => $this->price
        ]);

    }
}
