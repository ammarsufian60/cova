<?php


namespace App\Domains\Shipment\Actions;


use App\Domains\Branch\Models\Branch;
use App\Domains\Service\Models\Service;

class CalculateShipmentPrice
{
    /**
     * CalculateShipmentPrice constructor.
     * @param $branch_id
     * @param $latitude
     * @param $longitude
     */
    public function __construct($branch_id , $latitude,$longitude)
    {
        $this->latitude =$latitude;
        $this->longitude = $longitude;
        $this->branch =Branch::find($branch_id);
        $this->km_price = Service::where('service_name','delivery_km_price_SR')->first()->value;
        $this->base_delivery_price = Service::where('service_name','base_delivery_price')->first()->value;
        $this->price = 0;
    }

    /**
     * @return false|float|int
     */
    public function handle()
    {
        //get the destination location from branch data
        $LatDestination =  $this->branch->latitude;
        $LongDestination = $this->branch->longitude;

        //implement the Google distance api to calculate the  exactly distance between the user location and branch location.
        $distance  = ceil(distanceByGoogleDirections($this->latitude , $this->longitude , $LatDestination , $LongDestination));

           
//        if(is_null( $this->km_price)){
//            if($distance >= 4){
//                $sub_distance = $distance-4;
//                $this->price = 17 + $sub_distance;
//
//            }else{
//                $this->price = 17;
//            }
//        }else{
//            $this->price =$distance * $this->km_price + $this->base_delivery_price ;
//        }

        if($distance <=4){
            $this->price = $this->base_delivery_price;
        }else{
            $this->price =$distance * $this->km_price + $this->base_delivery_price;
        }

        return $this->price;
    }
}
