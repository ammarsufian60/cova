<?php


namespace App\Domains\Shipment;


use App\Domains\Shipment\Actions\CalculateShipmentPrice;
use App\Domains\Shipment\Actions\CreateShipment;

class ShipmentServices
{
    /**
     * @param $order_id
     * @return CreateShipment
     */
    public function create($order_id,$branch_id,$price =null)
    {
        return (new CreateShipment($order_id,$branch_id,$price))->handle();
    }

    /**
     * @param $branch_id
     * @param $latitude
     * @param $longitude
     * @return false|float|int
     */
    public function calculate($branch_id,$latitude,$longitude)
    {
        return ( new CalculateShipmentPrice($branch_id,$latitude,$longitude))->handle();
    }
}
