<?php


namespace App\Domains\Shipment\Http\Resource;

use App\Domains\User\Http\Resources\DriverShipmentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ShipmentResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'price' => $this->price,
           'driver' => new DriverShipmentResource($this->driver),
        ];
    }
}
