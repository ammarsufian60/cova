<?php


namespace App\Domains\Shipment\Http\Controller;

use App\Domains\Shipment\Http\Request\CalculateShipmentPriceRequest;
use App\Facades\Shipment;
use App\Http\Controllers\Controller;

class ShipmentController extends Controller
{
    /**
     * @param CalculateShipmentPriceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculate(CalculateShipmentPriceRequest $request)
    {
        try {
            $price = Shipment::calculate($request->branch_id, $request->latitude, $request->longitude);
        }catch (\Exception $e)
        {
            return response()->json(['message' => 'Something Wrong..'],500);
        }

       return response()->json(['message' =>(float) $price]);
    }
}
