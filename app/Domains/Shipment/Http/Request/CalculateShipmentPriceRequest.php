<?php


namespace App\Domains\Shipment\Http\Request;


use Illuminate\Foundation\Http\FormRequest;

class CalculateShipmentPriceRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return \string[][]
     */
    public function rules()
    {
        return[
            'branch_id' => ['required','exists:branches,id'],
        ];
    }
}
