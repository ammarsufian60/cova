<?php


namespace App\Domains\Cart;


use App\Domains\Cart\Actions\CreateCart;
use App\Domains\Cart\Actions\CreateCartItems;
use App\Domains\Cart\Actions\UpdateCartStatus;

class CartServices
{

    public function create()
    {
      return (new CreateCart())->handle();
    }

    public function createCartItem($items,$cart_id)
    {
        return (new CreateCartItems($items, $cart_id))->handle();
    }

    public function updateCartStatus($cart_id,$status)
    {
        return (new UpdateCartStatus($cart_id,$status))->handle();
    }





}
