<?php

namespace App\Domains\Cart\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    /**
     * id,user_id,status
     */
    protected $fillable =[
        'user_id',
        'status',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(CartItem::class);
    }

}
