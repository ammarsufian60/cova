<?php

namespace App\Domains\Cart\Models;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $fillable = [
        'cart_id',
        'buy_id',
        'buy_type',
        'price',
        'quantity',
        'parent_cart_item_id',
    ];

    public function buy()
    {
        return $this->morphTo();
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function parent()
    {
        return $this->belongsTo(self::class,'parent_cart_item_id');
    }
}
