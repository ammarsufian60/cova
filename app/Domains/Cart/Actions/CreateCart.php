<?php


namespace App\Domains\Cart\Actions;


use App\Domains\Cart\Models\Cart;
use Illuminate\Support\Facades\Auth;

class CreateCart
{

    public function __construct()
    {

    }

    public function handle()
    {
        $PendingCart = Auth::user()->carts->where('status' ,'pending')->first();
        if (is_null($PendingCart)) {
            return Cart::create([
                'user_id' => Auth::user()->id ,
                'status' => 'pending' ,
            ]);
        } else {
            return $PendingCart;
        }
    }
}
