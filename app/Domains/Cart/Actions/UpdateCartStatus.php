<?php


namespace App\Domains\Cart\Actions;


use App\Domains\Cart\Models\Cart;

class UpdateCartStatus
{
    protected $cart_id;
    protected $status;

    /**
     * UpdateCartStatus constructor.
     * @param $cart_id
     * @param $status
     */
    public function __construct($cart_id , $status)
    {
        $this->cart_id = $cart_id;
        $this->status = $status;
    }

    public function handle()
    {
        $cart = Cart::find($this->cart_id)->update(['status'=> $this->status]);

        return $cart;
    }

}
