<?php


namespace App\Domains\Cart\Actions;

use App\Domains\Product\Models\Product;
use App\Domains\Cart\Models\CartItem;
use App\Domains\Product\Models\AdditionalItem;
use App\Domains\Product\Models\ProductVariant;

class CreateCartItems
{

    protected $items=[];
    protected $cart_id;

    public function __construct( $items,$cart_id)
    {
      $this->items = json_decode($items);
      $this->cart_id = $cart_id;
 }

    public function handle()
    {

       collect($this->items)->each(function($item){

         $base_cart_item = $this->createProductCartItem($item->id,$item->amount);
        
         isset($item->size) ? $this->createProductVariantCartItem((array) $item->size,$base_cart_item->id) : null;

         isset($item->addition) ? $this->createProductAdditionalCartItem((array) $item->addition ,$base_cart_item->id):null;
       });

       return 'successful';

    }

    /**
     * @param $product_id
     * @return mixed
     */
    protected function createProductCartItem($product_id,$amount)
    {
        $product = Product::find($product_id);

        $cart_item = CartItem::create([
            'cart_id' => $this->cart_id,
            'buy_type' => 'Product',
            'buy_id' => $product->id,
            'price' => $product->price,
            'quantity' => $amount,
        ]);

        return $cart_item;
    }

    /**
     * @param array $size
     * @param $base_cart_item_id : is the id of base product cart item
     */
    protected function createProductVariantCartItem(array $size , $base_cart_item_id)
    {
        collect($size)->each(function($size) use($base_cart_item_id) {
        
            $product_variant = ProductVariant::find($size);

            CartItem::create([
                'cart_id' => $this->cart_id,
                'buy_type' => 'ProductVariant',
                'buy_id' => $product_variant->id,
                'price' => $product_variant->products->first()->pivot->price,
                'quantity' => 1,
                'parent_cart_item_id'=> $base_cart_item_id,
            ]);
        });
    }

    /**
     * @param array $addition
     * @param $base_cart_item_id : is the id of base product cart item
     */
    protected function createProductAdditionalCartItem(array $addition , $base_cart_item_id)
    {
        collect($addition)->each(function($addition) use($base_cart_item_id) {

            $additional = AdditionalItem::find($addition);

            CartItem::create([
                'cart_id' => $this->cart_id,
                'buy_type' => 'AdditionalItem',
                'buy_id' => $additional->id,
                'price' => $additional->products->first()->pivot->price,
                'quantity' => 1,
                'parent_cart_item_id'=> $base_cart_item_id,
            ]);

        });
    }
}
