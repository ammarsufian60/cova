<?php

namespace App\Domains\Product\Models;

use Illuminate\Database\Eloquent\Model;

class AdditionalItem extends Model
{
    protected $fillable =[
        'name',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class,'additional_items_product','additional_item_id','product_id')->withPivot('price');
    }
}
