<?php

namespace App\Domains\Product\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    protected $fillable= [
        'name',
        'key',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class,'products_product_variants', 'variant_id','product_id')->withPivot('price');

    }

}
