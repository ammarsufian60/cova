<?php

namespace App\Domains\Product\Models;

use App\Domains\Brand\Models\Brand;
use App\Domains\Category\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\File;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;


class Product extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable= [
        'name',
        'description',
        'price',
        'quantity',
        'is_available',
        'category_id',
        'brand_id',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function variants()
    {
      return $this->belongsToMany(ProductVariant::class,'products_product_variants','product_id','variant_id')->withPivot('price');
    }

    public function additional_items()
    {
        return $this->belongsToMany(AdditionalItem::class,'additional_items_product','product_id','additional_item_id')->withPivot('price');
    }


    public function registerMediaCollections()
    {
        $this->addMediaCollection('product')
            ->acceptsFile(function (File $file) {
                return in_array($file->mimeType, [
                    'image/jpeg',
                    'image/png',
                ]);
            })
            ->singleFile();
    }

    /**
     * @return string|null
     */
    public function getImageAttribute()
    {
        $image = $this->getFirstMediaUrl('product');
        return empty($image)? null :config('app.url') . $image;
    }

}
