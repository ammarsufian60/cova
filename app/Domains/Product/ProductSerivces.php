<?php


namespace App\Domains\Product;


use App\Domains\Product\Actions\GetProductById;
use App\Domains\Product\Actions\ProductsList;

class ProductSerivces
{
    /**
     * @return Models\Product[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index($category_id,$brand_id)
    {
        return (new ProductsList($category_id,$brand_id))->handle();
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function getProductById($product_id)
    {
        return (new GetProductById($product_id))->handle();
    }

}
