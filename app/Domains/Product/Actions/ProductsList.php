<?php


namespace App\Domains\Product\Actions;


use App\Domains\Product\Models\Product;

class ProductsList
{
    protected $category_id;
    protected $brand_id;

    public function __construct($category_id ,$brand_id)
    {
        $this->category_id = $category_id;
        $this->brand_id = $brand_id;
    }

    public function handle()
    {
        return Product::where('is_available',true)
            ->where('brand_id',$this->brand_id)
            ->where('category_id',$this->category_id)
            ->get();
    }
}
