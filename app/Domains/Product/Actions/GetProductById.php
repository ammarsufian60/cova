<?php


namespace App\Domains\Product\Actions;


use App\Domains\Product\Models\Product;

class GetProductById
{
    protected $product_id;

    /**
     * GetProductById constructor.
     * @param $product_id
     */
    public function __construct($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        return Product::find($this->product_id);
    }
}
