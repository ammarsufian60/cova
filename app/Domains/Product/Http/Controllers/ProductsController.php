<?php


namespace App\Domains\Product\Http\Controllers;


use App\Domains\Product\Http\Requests\ProductRequest;
use App\Domains\Product\Http\Resources\ProductResources;
use App\Domains\Product\ProductSerivces;
use App\Facades\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(ProductRequest $request)
    {
        $products = Product::index($request->category_id,$request->brand_id);

        return ProductResources::collection($products);
    }
}
