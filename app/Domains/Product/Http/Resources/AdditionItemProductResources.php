<?php


namespace App\Domains\Product\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class AdditionItemProductResources extends JsonResource
{
    public function toArray($request)
    {
       return [
          'id'=>$this->id,
           'name' => $this->name,
           'price'=> $this->pivot->price,
       ];
    }

}
