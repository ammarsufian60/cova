<?php


namespace App\Domains\Product\Http\Resources;


use App\Domains\Brand\Http\Resources\BrandResources;
use App\Domains\Category\Http\Resources\CategoryResources;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResources extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ,
            'name' => $this->name ,
            'description' => $this->description ,
            'price' => $this->price ,
            'quantity' => $this->quantity ,
            'image' => is_null($this->image) ? "" : $this->image ,
            'brand' => new BrandResources($this->brand) ,
            'category' => new  CategoryResources($this->category) ,
            'variants' => ProductVariantsResources::collection($this->variants) ,
            'addition_items' => AdditionItemProductResources::collection($this->additional_items) ,
        ];

    }

}
