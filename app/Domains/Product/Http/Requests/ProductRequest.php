<?php


namespace App\Domains\Product\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return \string[][]
     */
    public function rules()
    {
        return[
            'brand_id' => ['required','exists:brands,id'],
            'category_id' => ['required','exists:categories,id'],
        ];
    }
}
