<?php

namespace App\Domains\Category\Models;

use App\Domains\Branch\Models\Branch;
use App\Domains\Brand\Models\Brand;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Category extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable =[
        'name',
        'description',
        'parent_category_id',
        'status',
    ];

    public function branches()
    {
        return $this->belongsToMany(Branch::class,'branch_categories','category_id','branch_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class,'parent_category_id');
    }

    public function getImageAttribute()
    {
        $image = $this->getFirstMediaUrl('category');
        return empty($image)? null :config('app.url') . $image;
    }

}
