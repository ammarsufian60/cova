<?php


namespace App\Domains\Category\Actions;


use App\Domains\Category\Models\Category;
use App\Domains\Product\Models\Product;

class ListCategoriesByBrandId
{
 protected $brand_id;

 public function __construct($brand_id)
 {
     $this->brand_id = $brand_id;
 }

    /**
     * @return mixed
     */
    public function handle()
    {
        $categories_id = Product::where('brand_id',$this->brand_id)->pluck('category_id');

        return Category::whereIn('id',$categories_id)->get();
    }
}
