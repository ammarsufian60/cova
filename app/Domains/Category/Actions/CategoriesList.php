<?php


namespace App\Domains\Category\Actions;


use App\Domains\Category\Models\Category;

class CategoriesList
{

    public function __construct()
    {

    }
    public function handle()
    {
        return Category::whereNull('paren_category_id')->get();
    }
}
