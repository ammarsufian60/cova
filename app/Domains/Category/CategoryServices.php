<?php


namespace App\Domains\Category;


use App\Domains\Category\Actions\CategoriesList;
use App\Domains\Category\Actions\ListCategoriesByBrandId;

class CategoryServices
{
    /**
     * @return mixed
     */
    public  function  index()
    {
        return (new CategoriesList())->handle();
    }

    /**
     * @param $brand_id
     */
    public function getCategoriesByBrandId($brand_id)
    {
        return (new ListCategoriesByBrandId($brand_id))->handle();
    }
}
