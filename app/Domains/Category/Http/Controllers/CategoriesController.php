<?php


namespace App\Domains\Category\Http\Controllers;


use App\Domains\Category\Http\Resources\CategoryResources;
use App\Facades\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{

    public function index(Request $request)
    {
       $categories =  Category::index();

       return CategoryResources::collection($categories);
    }

    public function show(Request $request)
    {
      $this->validate($request,[
          'brand_id' => ['required' ]
      ]);

      $categories = Category::getCategoriesByBrandId($request->brand_id);

      return CategoryResources::collection($categories);


    }
}
