<?php


namespace App\Domains\Address\Actions;


use App\Domains\Address\Models\Address;
use Illuminate\Support\Facades\Auth;

class StoreUserAddress
{

    protected $params =[];

    /**
     * StoreUserAddress constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params =$params;
    }

    public function handle()
    {
        $address = Address::create([
            'type' => is_null($this->params['type'])? null : $this->params['type'] ,
            'latitude' => $this->params['latitude'],
            'longitude' => $this->params['longitude'],
            'city' => $this->params['city'],
            'area' => $this->params['area'],
            'is_default' => $this->params['is_default'],
            'user_id' => Auth::user()->id,
        ]);

        return $address;
    }
}
