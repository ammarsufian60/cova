<?php


namespace App\Domains\Address\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class AddressResources extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'type' => $this->type,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'city' => $this->city,
            'area' => $this->area,
            'is_default' => $this->is_default,
        ];
    }
}

