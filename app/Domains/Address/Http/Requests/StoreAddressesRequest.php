<?php


namespace App\Domains\Address\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class StoreAddressesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city' => ['required','string'],
            'area' => ['required','string'],
            'latitude' => ['required','string'],
            'longitude' => ['required','string'],
            'type' => ['required','string'],
            'is_default' => ['required','boolean'],
        ];
    }
}
