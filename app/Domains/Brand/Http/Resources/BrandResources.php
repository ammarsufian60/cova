<?php


namespace App\Domains\Brand\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class BrandResources extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,
            'name' => $this->name,
            'description' =>$this->description,
            'image' => $this->image,
        ];
    }
}
