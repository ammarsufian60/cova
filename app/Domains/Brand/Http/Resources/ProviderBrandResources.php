<?php


namespace App\Domains\Brand\Http\Resources;


use App\Domains\Product\Http\Resources\CustomProductResources;
use Illuminate\Http\Resources\Json\JsonResource;

class ProviderBrandResources extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,
            'name' => $this->name,
            'description' =>$this->description,
            'image' => $this->image,
            'products' => CustomProductResources::collection($this->products)
        ];
    }
}
