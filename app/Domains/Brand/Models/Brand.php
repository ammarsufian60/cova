<?php

namespace App\Domains\Brand\Models;

use App\Domains\Branch\Models\Branch;
use App\Domains\Category\Models\Category;
use App\Domains\Product\Models\Product;
use App\Domains\Rating\Models\Rating;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Laravel\Nova\Fields\HasMany;
use Spatie\MediaLibrary\File;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Brand extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable =[
        'name',
        'user_id',
        'description',
        'status',
        'is_international',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function branches()
    {
      return $this->hasMany(Branch::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
    public function registerMediaCollections()
    {
        $this->addMediaCollection('brand')
            ->acceptsFile(function (File $file) {
                return in_array($file->mimeType, [
                    'image/jpeg',
                    'image/png',
                ]);
            })
            ->singleFile();
    }
    public function getImageAttribute()
    {
        $image = $this->getFirstMediaUrl('brand');
        return empty($image)? null :config('app.url') . $image;
    }



}
