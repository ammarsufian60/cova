<?php


namespace App\Domains\User\Actions;


use App\User;
use Illuminate\Support\Facades\Hash;

class LoginUser
{
    protected $params;

    /**
     * LoginUser constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $user =User::where('mobile_number', $this->params['mobile_number'])->first();

        if(! Hash::check($this->params['password'] , $user->password))
        {
            throw new \Exception('اسم المستخدم او كلمة المرور غير صحيحة');
        }

        (new SendOTP($user->mobile_number))->handle();

        return $user;
    }

}
