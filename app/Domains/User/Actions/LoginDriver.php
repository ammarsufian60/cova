<?php


namespace App\Domains\User\Actions;


use App\User;
use Illuminate\Support\Facades\Hash;

class LoginDriver
{
    /**
     * LoginDriver constructor.
     * @param $mobile_number
     * @param $password
     */
    public function __construct($mobile_number , $password)
    {
        $this->mobile_number = $mobile_number;
        $this->password = $password;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $user =User::where('mobile_number', $this->mobile_number)->first();


        if(!$user->hasRole('driver'))
        {
            throw new \Exception('نعتذر هذا التطبيق مخصص لسائقين فقط');
        }

        if(! Hash::check($this->password, $user->password))
        {
            throw new \Exception('اسم المستخدم او كلمة المرور غير صحيحة');
        }

        $user->update(['status' => 'available']);

        return $user;
    }
}
