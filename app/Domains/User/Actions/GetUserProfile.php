<?php


namespace App\Domains\User\Actions;


use App\User;
use Illuminate\Support\Facades\Auth;

class GetUserProfile
{

    protected $user_id;
    /**
     * GetUserProfile constructor.
     */
    public function __construct()
    {
        $this->user_id  = Auth::user()->id;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $user = User::with('orders')->find($this->user_id);

        return $user;
    }
}
