<?php


namespace App\Domains\User\Actions;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangeUserPassword
{
    protected $current_password;
    protected $new_password;

    /**
     * ChangeUserPassword constructor.
     * @param $current_password
     * @param $new_password
     */
    public function __construct($current_password, $new_password)
    {
        $this->current_password = $current_password;
        $this->new_password = $new_password;
        $this->user = Auth::user();
//       dd($this->user);
    }

    public function handle()
    {
         
      if (Auth::guard('web')->attempt(['mobile_number'=> (string) $this->user->mobile_number,'password'=>$this->current_password])) {
          //  throw new \Exception('Something wrong Please check your data');
       $this->user->update(['password' => Hash::make($this->new_password)]);  
      }
	else{
 	    throw new \Exception('Something wrong Please check your data');
	}

        //$this->user->update(['password' => Hash::make($this->new_password)]);

      return 'success';
    }

}
