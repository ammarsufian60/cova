<?php


namespace App\Domains\User\Actions;


use App\User;
use Illuminate\Support\Facades\Hash;

class RegisterDriver
{

    public function __construct(array $params)
    {
        $this->params= $params;
    }

    public function handle()
    {
        $user = User::create([
            'name' => $this->params['name'],
            'password' => Hash::make($this->params['password']),
            'email' => $this->params['email'],
            'mobile_number' =>$this->params['mobile_number'],
            'role' => 'driver'
        ]);

        $user->assignRole('driver');

        return $user;
    }
}
