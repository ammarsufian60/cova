<?php


namespace App\Domains\User\Actions;


use App\User;
use Illuminate\Support\Facades\Auth;

class UpdateUserProfile
{
    protected $data;

    /**
     * UpdateUserProfile constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->user_id = Auth::user()->id;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $user = User::find($this->user_id);

        $user->mobile_number = $this->data[ 'mobile_number' ];
        $user->name = $this->data[ 'user_name' ];
        $user->email = $this->data[ 'email' ];

        $user->save();

        return $user;
    }

}
