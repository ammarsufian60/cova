<?php


namespace App\Domains\User\Actions;

use App\User;
use Illuminate\Support\Facades\Hash;

class RegisterUser
{
    protected $params;

    /**
     * RegisterUser constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params =$params;
    }

    /**
     * @return mixed
     */
    public function  handle()
    {
        $user = User::create([
            'name' => $this->params['name'],
            'password' => Hash::make($this->params['password']),
            'email' => $this->params['email'],
            'mobile_number' =>$this->params['mobile_number'],
            'role' => 'application'
        ]);
     
        $user->assignRole('application');

        (new SendOTP($user->mobile_number))->handle();

        return $user;

   }
}
