<?php


namespace App\Domains\User\Actions;


use Shreifelagamy\Hisms\HismsFacade;

class SendSMSMessage
{
    /**
     * SendSMSMessage constructor.
     * @param $mobile_number
     * @param string $message
     */
    public function __construct($mobile_number, $message='طلب جديد')
    {
        $this->mobile_number = $mobile_number;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function handle()
    {
        HismsFacade::sendSMS( $this->message, $this->mobile_number);

        return 'success';
    }
}
