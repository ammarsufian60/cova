<?php


namespace App\Domains\User\Actions;


use App\User;
use Illuminate\Support\Facades\Hash;

class ForgetPassword
{

    protected $params;

    /**
     * ForgetPassword constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $user = User::where('mobile_number', $this->params[ 'mobile_number' ])->first();

        if (!$user->is_verified) {
            throw new \Exception('ادخل رمز التآكيد');
        }
        $user->update([
            'password' => Hash::make($this->params[ 'new_password' ])
        ]);


        return $user;
    }
}
