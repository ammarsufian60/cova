<?php


namespace App\Domains\User\Actions;


use Illuminate\Support\Facades\Auth;

class UpdateDriverStatus
{
    /**
     * UpdateDriverStatus constructor.
     * @param $status
     */
    public function __construct($status)
    {
        $this->status = statusDictionary($status);
    }

    /**
     * @return mixed
     */
    public function handle()
    {
       Auth::user()->update(['status' =>$this->status]);

       return 'success';
    }


}
