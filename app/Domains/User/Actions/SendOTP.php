<?php


namespace App\Domains\User\Actions;


use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Nexmo\Client\Credentials\Basic;
use Nexmo\Client;
use Shreifelagamy\Hisms\HismsFacade;

class SendOTP
{
    /**
     * @var
     */
    protected $mobile_number;
    protected $otp;

    public function __construct($mobile_number )
    {
        $this->client =new \GuzzleHttp\Client(['verify' => false]);
        $this->mobile_number = $mobile_number;
        $this->otp = mt_rand(1000,9999);
    }

    /**
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function handle()
    {

            $user =User::where('mobile_number',$this->mobile_number)->first();

//            $this->otp = $user->last_otp;

//           if(!(strpos($user->mobile_number, '966') === 0))
//            {
//                throw new \Exception('متوفر للارقام السعوديه فقط');
//            }

            if(Carbon::now()->subMinute(1)->toDateTimeString() <= $user->last_otp_at )
            {
                throw new \Exception('انتظر 40 ثانية قبل اعادة المحاولة .');
            }

            $user->update([
                'last_otp' =>$this->otp,
                'is_verified'=> false,
                'last_otp_at'=>Carbon::now(),
            ]);

            HismsFacade::sendSMS('مرحبا بك ,رمز التفعيل  :'. $this->otp .' لا تشاركه مع احد ', $user->mobile_number);


        return 'success';
    }

}
