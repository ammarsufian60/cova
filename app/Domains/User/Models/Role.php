<?php

namespace App\Domains\User\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Role extends Model
{
    protected $table='roles';

    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'role_has_permissions','role_id','permission_id');
    }
//model_has_permissions'

    public function users()
    {
        return $this->belongsToMany(User::class,'model_has_roles','role_id','model_id')->withPivot('model_type');
    }
}
