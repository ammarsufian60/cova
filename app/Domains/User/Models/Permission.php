<?php

namespace App\Domains\User\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table ='permissions';

    public function roles()
    {
        return $this->belongsToMany(Permission::class,'role_has_permissions','role_id');
    }
}
