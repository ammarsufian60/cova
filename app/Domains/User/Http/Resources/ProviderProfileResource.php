<?php

namespace App\Domains\User\Http\Resources;

use App\Domains\Branch\Http\Resources\BranchProfileResource;
use App\Domains\Branch\Http\Resources\BranchResources;
use Illuminate\Http\Resources\Json\JsonResource;

class ProviderProfileResource extends  JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'user_name' =>$this->name,
            'email' => $this->email,
            'mobile_number' =>$this->mobile_number,
            'branch' => new BranchProfileResource($this->branch),
        ];
    }
}
