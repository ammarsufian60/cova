<?php


namespace App\Domains\User\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class UserProfileResource extends  JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'user_name' =>$this->name,
            'email' => $this->email,
            'mobile_number' =>$this->mobile_number,
            'orders'=> $this->orders->count(),
	        'points'=>$this->points==0 ? "0" : $this->points,
        ];
    }
}
