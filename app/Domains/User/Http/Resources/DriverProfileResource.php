<?php


namespace App\Domains\User\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class DriverProfileResource extends  JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'user_name' =>$this->name,
            'email' => $this->email,
            'mobile_number' =>$this->mobile_number,
            'accepted_shipments' => $this->accepted_shipments,
            'rejected_shipments' => $this->rejected_shipments,
            'completed_shipments' => $this->completed_shipments,
            'iban' => $this->iban,
            'bank_name' => $this->bank_name,
            'revenue' => $this->shipments->where('status','delivered')->sum('price'),
        ];
    }
}

