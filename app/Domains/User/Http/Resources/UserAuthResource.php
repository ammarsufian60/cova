<?php


namespace App\Domains\User\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

class UserAuthResource extends  JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'user_name' =>$this->name,
            'email' => $this->email,
            'access_token' => $this->createToken('user')->accessToken,
            'orders'=> $this->orders->count(),
            'points' => is_null($this->points) ? 0 : $this->points,
        ];
    }
}
