<?php


namespace App\Domains\User\Http\Resources;

use App\Domains\Rating\Models\Rating;
use App\Domains\Shipment\Models\Shipment;
use Illuminate\Http\Resources\Json\JsonResource;


class DriverShipmentResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ,
            'driver_name' => $this->name ,
            'email' => $this->email ,
            'accepted_shipment' => is_null($this->accepted_shipments) ? 0 : $this->accepted_shipments ,
            'rejected_shipments' => is_null($this->rejected_shipments) ? 0 : $this->rejected_shipments ,
            'completed_shipments' => is_null($this->completed_shipments) ? 0 : $this->completed_shipments ,
            'rating' => $this->getDriverRatings($this->id) ,
        ];
    }

    /**
     * @param $driver_id
     * @return int
     */
    public function getDriverRatings($driver_id)
    {
        $summation_of_ratings = 0;
        $order_ids = Shipment::where('driver_id' ,$driver_id)->pluck('order_id');
        if (is_null($order_ids)) {
            $summation_of_ratings = Rating::whereHas('order' ,function ($query) use ($order_ids) {
                $query->whereIn('id' ,$order_ids);
            })->whereNotNull('driver_rating')->avg('driver_rating');

        }
        return $summation_of_ratings;

    }
}


