<?php


namespace App\Domains\User\Http\Controllers;

use App\Domains\User\Http\Resources\ProviderProfileResource;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class ProviderController extends Controller
{
    /**
     * @return ProviderProfileResource
     */
    public function profile()
    {
        $user_id = User::find(Auth::user()->id);
        return new ProviderProfileResource($user_id);
    }
}
