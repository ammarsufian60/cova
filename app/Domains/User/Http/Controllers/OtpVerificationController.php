<?php


namespace App\Domains\User\Http\Controllers;


use App\Domains\User\Actions\SendOTP;
use App\Domains\User\Http\Request\OtpVerificationRequest;
use App\Domains\User\Http\Request\SendOTPRequest;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class OtpVerificationController extends Controller
{
    /**
     * @param OtpVerificationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify(OtpVerificationRequest  $request)
    {
       try{
           $user = User::where('mobile_number' , Auth::user()->mobile_number)->first();

           if($user->last_otp !== $request->otp)
           {
             throw  new \Exception('. رمز تفعيل خاطئ حاول مرة اخرى');
           }
           $user->update(['is_verified' => true]);

       }catch(\Exception $e)
       {
           return response()->json(['message' => $e->getMessage()],500);
       }

        return response()->json(['message' => 'success']);
    }

    /**
     * @param SendOTPRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendOtp(SendOTPRequest $request)
    {
        try{
            $response =(new SendOTP($request->mobile_number))->handle();
        }catch(\Exception $e)
        {
            return response()->json(['message' => $e->getMessage()],500);
        }

        return response()->json(['message' => 'true']);
    }

}
