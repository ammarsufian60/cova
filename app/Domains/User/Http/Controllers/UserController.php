<?php


namespace App\Domains\User\Http\Controllers;


use App\Domains\User\Actions\ChangeUserPassword;
use App\Domains\User\Actions\GetUserProfile;
use App\Domains\User\Actions\UpdateUserProfile;
use App\Domains\User\Http\Request\ChangePasswordRequest;
use App\Domains\User\Http\Request\UpdateUserProfileRequest;
use App\Domains\User\Http\Resources\UserAuthResource;
use App\Domains\User\Http\Resources\UserProfileResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        try {
            (new ChangeUserPassword($request->current_password, $request->new_password))->handle();

        } catch (\Exception $e) {
            return response()->json([
                'message'=>$e->getMessage()
            ],500);
        }

        return response()->json(['message'=>'success'],200);
    }

    /**
     * @param Request $request
     * @return UserProfileResource
     */
    public function userProfile(Request $request)
    {
       $profile = (new GetUserProfile())->handle();

       return new UserProfileResource($profile);

    }

    public function updateProfile(UpdateUserProfileRequest $request)
    {
        $profile = (new UpdateUserProfile($request->only('user_name','email','mobile_number')))->handle();

        return response()->json(['message' => 'successful']);
    }
}
