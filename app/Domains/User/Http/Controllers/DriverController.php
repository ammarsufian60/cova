<?php

namespace App\Domains\User\Http\Controllers;

use App\Domains\User\Actions\LoginDriver;
use App\Domains\User\Actions\RegisterDriver;
use App\Domains\User\Actions\UpdateDriverStatus;
use App\Domains\User\Http\Request\LoginRequest;
use App\Domains\User\Http\Request\RegisterRequest;
use App\Domains\User\Http\Resources\DriverProfileResource;
use App\Domains\User\Http\Resources\UserAuthResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DriverController extends Controller
{
    /**
     * @param LoginRequest $request
     * @return UserAuthResource|\Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        try{
            $user = (new LoginDriver($request->mobile_number,$request->password))->handle();

        }catch (\Exception $e)
        {
            return response()->json(['message' => $e->getMessage()],500);
        }

        return new UserAuthResource($user);
    }

    /**
     * @param RegisterRequest $request
     * @return UserAuthResource|\Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        try{
            $response = (new RegisterDriver($request->all()))->handle();

        }catch(\Exception $e)
        {
            return response()->json(['message' =>$e->getMessage()], 500 );
        }

        return new UserAuthResource($response);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateDriverStatus(Request $request)
    {
        $request->validate([
            'status' => ['required']
        ]);

        try{
            $response =(new UpdateDriverStatus($request->status));

        }catch(\Exception $e){
            return response()->json(['message' => $e->getMessage()],500);
        }

        return response()->json(['message' => 'success']);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addBankDetails(Request $request)
    {
        $request->validate([
           'iban' => ['required'] ,
            'bank_name' => ['required'],
        ]);

        try{

            Auth::user()->update(['iban' => $request->iban,'bank_name'=> $request->bank_name]);

        }catch (\Exception $e){
            return response()->json(['message' => 'Something Wrong ..'],500);
        }

        return response()->json(['message' => 'success']);
    }

    public function DriverProfile(Request $request)
    {
        try{
            $driver = Auth::user();

        }catch(\Exception $e)
        {
            return response()->json(['message'=>$e->getMessage()],500);
        }

        return new DriverProfileResource($driver);
    }
}
