<?php


namespace App\Domains\User\Http\Controllers;

use App\Domains\User\Actions\ForgetPassword;
use App\Domains\User\Actions\LoginUser;
use App\Domains\User\Actions\RegisterUser;
use App\Domains\User\Http\Request\ForgetPasswordRequest;
use App\Domains\User\Http\Request\LoginRequest;
use App\Domains\User\Http\Request\RegisterRequest;
use App\Domains\User\Http\Resources\UserAuthResource;
use App\Http\Controllers\Controller;


class AuthController extends Controller
{
    /**
     * @param LoginRequest $request
     * @return UserAuthResource|\Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        try{
           $response = (new LoginUser($request->all()))->handle();

        }catch (\Exception $e)
        {
            return response()->json(['message' => $e->getMessage()],500);
        }


        return new UserAuthResource($response);
    }

    /**
     * @param RegisterRequest $request
     * @return UserAuthResource|\Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        try{
             $response = (new RegisterUser($request->all()))->handle();
        }catch(\Exception $e)
        {
            return response()->json(['message' =>$e->getMessage()], 500 );
        }

        return new UserAuthResource($response);
    }

    /**
     * @param ForgetPasswordRequest $request
     * @return UserAuthResource|\Illuminate\Http\JsonResponse
     */
    public function forgetPassword(ForgetPasswordRequest $request)
    {
        try{
            $response = (new ForgetPassword($request->all()))->handle();
        }catch(\Exception $e)
        {
            return response()->json(['message' =>$e->getMessage()],500);
        }
        return new UserAuthResource($response);
    }

}
