<?php


namespace App\Domains\User\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required','email','unique:users,email'],
            'password' => ['required','min:8'],
            'name' => ['required','string','min:3'],
            'mobile_number' => ['required','unique:users,mobile_number'], //'startswith:966'],
        ];
    }
}
