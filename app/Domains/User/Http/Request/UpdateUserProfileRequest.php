<?php


namespace App\Domains\User\Http\Request;


use Illuminate\Foundation\Http\FormRequest;

class UpdateUserProfileRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return \string[][]
     */
    public function rules()
    {
        return [
            'mobile_number' => ['required'],
            'user_name' => ['required', 'string', 'min:5'],
            'email' => ['required', 'email'],
        ];
    }



}
