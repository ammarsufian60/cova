<?php


namespace App\Domains\User\Http\Request;


use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return \string[][]
     */
    public function rules()
    {
        return [
            'current_password' =>['required','min:8'],
            'new_password' =>['required','min:8','confirmed'],
        ];
    }
}
