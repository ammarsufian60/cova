<?php

namespace App\Domains\Order\Models;

use App\Domains\Cart\Models\Cart;
use App\Domains\Cart\Models\CartItem;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable =[
	    'id',
        'order_id',
        'buy_id',
        'buy_type',
        'price',
        'quantity',
        'cart_item_id',
        'parent_order_item_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function buy()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cart_item()
    {
        return $this->belongsTo(CartItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
   public function item_details()
   {
      return $this->hasMany(OrderItem::class , 'parent_order_item_id');
   }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function additional_items()
    {
        return  $this->hasMany(OrderItem::class , 'parent_order_item_id');
         
       return is_null($items)?null : $items->where('buy_type','AdditionalItem');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function product_variants()
    {
        return $this->hasMany(OrderItem::class , 'parent_order_item_id');
    }


}
