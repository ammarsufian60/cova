<?php


namespace App\Domains\Order;


use App\Domains\Order\Actions\CreateOrder;
use App\Domains\Order\Actions\CreateOrderItem;
use App\Domains\Order\Actions\DriverAcceptOrder;
use App\Domains\Order\Actions\DriverChangeOrderStatus;
use App\Domains\Order\Actions\DriverOrderList;
use App\Domains\Order\Actions\FilterBranchOrders;
use App\Domains\Order\Actions\GetOrderById;
use App\Domains\Order\Actions\LatestOrder;
use App\Domains\Order\Actions\OrderHistory;
use App\Domains\Order\Actions\SetOrderStatusInroute;
use App\Domains\Order\Http\Requests\DriverAcceptOrderRequest;

class OrderServices
{
    /**
     * @param $cart_id
     * @param $address_id
     * @param $branch_id
     * @param $total
     * @return mixed
     */
    public function create($cart_id , $address_id,$branch_id,$total)
    {
       return (new CreateOrder($cart_id,$address_id,$branch_id,$total))->handle();
    }

    /**
     * @return mixed
     */
    public function index($status)
    {
        return (new OrderHistory($status))->handle();
    }

    /**
     * @param $order_id
     * @param $cart_id
     * @return string
     */
    public function createOrderItem($order_id,$cart_id)
    {
        return (new CreateOrderItem($order_id , $cart_id))->handle();
    }

    /**
     * @return mixed
     */
    public function lastestOrder()
    {
        return (new LatestOrder())->handle();
    }

    /**
     * @param $branch_id
     * @param $status
     * @param $from_date
     * @param $to_date
     * @return mixed
     */
    public function filterBranchOrders($branch_id,$status,$from_date,$to_date)
    {
       return ( new FilterBranchOrders($branch_id,$status,$from_date,$to_date))->handle();
    }

    /**
     * @param $order_id
     * @param $status
     * @return string
     * @throws \Exception
     */
    public function DriverAcceptOrder($order_id,$status)
    {
        return (new DriverAcceptOrder($order_id,$status))->handle();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function DriverOrdersList(array $params)
    {
        return (new DriverOrderList($params))->handle();
    }

    /**
     * @param $order_id
     * @param $status
     * @return string
     * @throws \Exception
     */
    public function UpdateOrderStatus($order_id,$status)
    {
       return (new DriverChangeOrderStatus($order_id,$status))->handle();
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function getOrderById($order_id)
    {
        return (new GetOrderById($order_id))->handle();
    }

    /**
     * @param $order_id
     * @return mixed
     */
    public function setOrderStatusInRoute($order_id)
    {
        return (new SetOrderStatusInroute($order_id))->handle();
    }

}
