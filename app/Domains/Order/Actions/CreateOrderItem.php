<?php


namespace App\Domains\Order\Actions;


use App\Domains\Cart\Models\Cart;
use App\Domains\Order\Models\OrderItem;

class CreateOrderItem
{
    protected $order_id;
    protected $cart_id;

    /**
     * CreateOrderItem constructor.
     * @param $order_id
     * @param $cart_id
     */
    public function __construct($order_id,$cart_id)
    {
        $this->order_id = $order_id;
        $this->cart_id =$cart_id;
        $this->parent_id =null;
    }
    public function handle()
    {
//	dd(Cart::find($this->cart_id)->items->count())
        Cart::find($this->cart_id)->items->each(function($item){
          
       // $child_items =   CartItem::where('parent_cart_item_id',$item->id);
       // collect($child_items)->each(function($item){
         
            $order_item =  OrderItem::create([
                'id' => $item->id,
                'order_id' => $this->order_id,
                'buy_id' => $item->buy_id,
                'buy_type' => $item->buy_type,
                'price' => $item->price,
                'quantity'=> $item->quantity,
                'cart_item_id' => $item->id,
                'parent_order_item_id' =>$item->parent_cart_item_id               
            ]);
         // }
         });

        return 'done';
    }
}
