<?php


namespace App\Domains\Order\Actions;


use App\Domains\Order\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class FilterBranchOrders
{
    /**
     * FilterBranchOrders constructor.
     * @param $branch_id
     * @param $status
     * @param $from_date
     * @param $to_date
     */
    public function __construct($branch_id,$status,$from_date=null,$to_date=null)
    {
      $this->branch_id = $branch_id ;
      $this->status = $status ;
      $this->from_date = $from_date ;
      $this->to_date = $to_date ;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $branch =Auth::user()->branch;

        if(is_null($branch) && $branch->id == $this->branch_id){
            throw new \Exception('يرجى التحقق من وجود الفرع الخاص بك');
        }

        $orders = Order::where('branch_id',$this->branch_id)
              ->where('status',$this->status);

        if(!is_null($this->from_date)){
            $orders->whereDate('created_at','>', Carbon::parse($this->from_date));
        }

        if(!is_null($this->to_date)){
            $orders->whereDate('created_at','<', Carbon::parse($this->from_date));
        }

        return $orders->get();

    }
}
