<?php


namespace App\Domains\Order\Actions;


use App\Domains\Order\Models\Order;
use App\Domains\User\Http\Resources\UserAuthResource;
use Illuminate\Support\Facades\Auth;

class DriverChangeOrderStatus
{
    /**
     * @param $order_id
     * @param $status
     */
    public function __construct($order_id,$status)
    {
      $this->order_id = $order_id;
      $this->status   = $status;
      $this->driver   = Auth::user();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function handle()
    {

        $last_shipment = Auth::user()->shipments->sortByDesc('created_at')->first();
        if($last_shipment->order_id !== $this->order_id && $last_shipment->status == 'delivered')
        {
            throw new \Exception('Update Shipment status failed .. ');
        }

        $last_shipment->update(['status' => $this->status =='in-shop' ?'in-route':$this->status]);

        if($this->status == 'delivered')
        {
            $this->driver->update(['completed_shipments' => $this->driver->completed_shipments + 1]);
        }

        if($this->status == 'out-for-delivery')
        {
            $this->status = 'in-route';
        }
       
        Order::find($this->order_id)->update(['status' => $this->status]);

        return 'success' ;
    }

}
