<?php


namespace App\Domains\Order\Actions;


use App\Domains\Order\Models\Order;
use Illuminate\Support\Facades\Auth;

class OrderHistory
{
    /**
     * OrderHistory constructor.
     * @param $status
     */
    public function __construct($status)
    {
        $this->status = $status;
        $this->user_id = Auth::user()->id;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $orders = null;
        if(is_null($this->status))
        {
            $orders = Order::where('user_id', $this->user_id)->get();
        }else{
            $orders = Order::where('user_id', $this->user_id)->where('status',$this->status)->get();
        }

        return $orders;
    }
}
