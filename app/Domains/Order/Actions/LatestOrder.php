<?php


namespace App\Domains\Order\Actions;


use App\Domains\Order\Models\Order;
use Illuminate\Support\Facades\Auth;

class LatestOrder
{
    /**
     * LatestOrder constructor.
     */
    public function __construct()
    {
        $this->user_id = Auth::user()->id;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::where('user_id', $this->user_id)->orderby('created_at','DESC')->first();

  /*      if(is_null($orders))
        {
         throw new \Exception('false');
         }
*/
        return $orders;
    }
}
