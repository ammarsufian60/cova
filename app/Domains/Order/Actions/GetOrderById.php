<?php


namespace App\Domains\Order\Actions;


use App\Domains\Order\Models\Order;

class GetOrderById
{
    /**
     * GetOrderById constructor.
     * @param $order_id
     */
    public function __construct($order_id)
    {
        $this->order_id = $order_id;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        return Order::find($this->order_id);
    }
}
