<?php


namespace App\Domains\Order\Actions;


use App\Domains\Order\Models\Order;
use App\Facades\BranchFacade;

class DriverOrderList
{
    public function __construct(array $params)
    {
        $this->params = $params;

        $this->branch_ids = BranchFacade::nearby($this->params['latitude'],$this->params['longitude'],0,20)->pluck('id');
  }

    public function handle()
    {
        $orders = Order::where('status','pending')->whereIn('branch_id',$this->branch_ids)->get();

        return $orders;
    }

}
