<?php


namespace App\Domains\Order\Actions;


use App\Domains\Order\Models\Order;
use Illuminate\Support\Facades\Auth;

class DriverAcceptOrder
{
    /**
     * DriverAcceptOrder constructor.
     * @param $order_id
     * @param $status
     */
    public function __construct($order_id,$status)
    {
      $this->order= Order::find($order_id);
      $this->status = $status;
      $this->driver= Auth::user();

      if($this->status){
          $this->driver->update(['accepted_shipments' => $this->driver->accepted_shipments +1]);

      }else{
          $this->driver->update(['rejected_shipments' => $this->driver->rejected_shipments +1]);
      }

    }

    /**
     * @return string
     * @throws \Exception
     */
    public function handle()
    {
       
        if($this->order->status !== 'pending' && !is_null($this->order->shipments->first()->driver_id))
        {
            throw new \Exception('نعتذر لقد تم ارفاق الطلب لدى سائق اخر');
        } 
     
           
         $this->order->update(['status'=>'accepted']);
  
        $this->order->shipments->first()->update(['driver_id'=>$this->driver->id,'status' =>'out-for-delivery']);

        return 'success';
    }
}
