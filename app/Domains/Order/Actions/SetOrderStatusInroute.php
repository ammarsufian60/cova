<?php


namespace App\Domains\Order\Actions;


use App\Domains\Order\Models\Order;

class SetOrderStatusInroute
{
    /**
     * SetOrderStatusInroute constructor.
     * @param $order_id
     */
    public function __construct($order_id)
    {
        $this->order = Order::find($order_id);
    }
    /**
     * @return mixed
     */
    public function handle()
    {
       return  $this->order->update(['status'=>'in-route']);
    }
}
