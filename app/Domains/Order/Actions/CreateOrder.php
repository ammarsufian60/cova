<?php


namespace App\Domains\Order\Actions;


use App\Domains\Order\Models\Order;
use App\Facades\Cart;
use Illuminate\Support\Facades\Auth;

class CreateOrder
{
    /**
     * CreateOrder constructor.
     * @param $cart_id
     * @param $address_id
     * @param $branch_id
     * @param $total
     */
    public function __construct($cart_id, $address_id,$branch_id,$total)
    {
      $this->cart_id = $cart_id;
      $this->address_id = $address_id;
      $this->branch_id = $branch_id;
      $this->total =$total;
    }

    /**
     * @return mixed
     */
    public function  handle()
    {
       $order =  Order::create([
            'user_id' => Auth::user()->id,
            'address_id'=> $this->address_id,
            'cart_id' => $this->cart_id,
            'status' => 'pending',
            'branch_id'=>$this->branch_id,
            'total' => $this->total,
        ]);


       Cart::updateCartStatus($this->cart_id,'reserved');


       return $order;
    }
}
