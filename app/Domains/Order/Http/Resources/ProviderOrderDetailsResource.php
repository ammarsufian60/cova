<?php


namespace App\Domains\Order\Http\Resources;


use App\Domains\Address\Http\Resources\AddressResources;
use App\Domains\Shipment\Http\Resource\ShipmentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProviderOrderDetailsResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'items' => OrderItemResource::collection($this->items),
            'total' => $this->total,
            'created_at_date' => \Carbon\Carbon::parse($this->created_at)->format('y-m-d'),
            'estimation_delivery_time'=> $this->branch->estimate_delivery_time,
            'address' => new AddressResources($this->address),
        ];
    }

}
