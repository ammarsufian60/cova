<?php


namespace App\Domains\Order\Http\Resources;


use App\Domains\Address\Http\Resources\AddressResources;
use App\Domains\Product\Models\AdditionalItem;
use App\Domains\Product\Models\Product;
use App\Domains\Product\Models\ProductVariant;
use App\Domains\Shipment\Http\Resource\ShipmentResource;
use App\Domains\User\Http\Resources\DriverShipmentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderHistoryResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'address' => new AddressResources($this->address),
            'status' => $this->orderStatus($this->status),
            'total' => $this->transaction->amount,
            'payment_method' => $this->transaction->payment_method,
            'created_at' => \Carbon\Carbon::parse($this->created_at)->format('y-m-d'),
            'items' => OrderItemResource::collection($this->items),
            'branch_id' => $this->branch_id,
            'brand_name' => $this->branch->first()->brand->name,
            'brand_image' => $this->branch->first()->brand->image,
            'shipments' => new ShipmentResource($this->shipments->first())
        ];
    }

    /**
     * @param $status
     * @return string
     */
    public function orderStatus($status)
    {
        switch ( $status ) {
            case 'pending':
            case 'waiting-for-acceptance':
                $status = 'pending';
                break;
            case 'in-route':
            case 'in-shop':
                $status = 'in-route';
                break;
            case 'accepted':
                $status = 'accepted';
                break;
            case 'delivered':
                $status = 'delivered';
                break;
            default:
                $status = 'canceled';

        }
        return $status;
    }

}
