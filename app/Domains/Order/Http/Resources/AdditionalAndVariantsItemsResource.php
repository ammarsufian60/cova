<?php


namespace App\Domains\Order\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class AdditionalAndVariantsItemsResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
       
      return [
         'name' => $this->buy->name,
         'quantity' => $this->quantity,
      ];
    }
}
