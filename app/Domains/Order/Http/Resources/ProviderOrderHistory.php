<?php


namespace App\Domains\Order\Http\Resources;


use App\Domains\Address\Http\Resources\AddressResources;
use App\Domains\Shipment\Http\Resource\ShipmentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProviderOrderHistory extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->orderStatus($this->status),
            'created_at_time' => \Carbon\Carbon::parse($this->created_at)->format('H:i:s'),
            'order_items_count' => $this->items->count(),
            'estimation_delivery_time'=> $this->branch->estimate_delivery_time,
            'shipments' => new ShipmentResource($this->shipments->first())
        ];
    }

    /**
     * @param $status
     * @return string
     */
    public function orderStatus($status)
    {
        switch ( $status ) {
            case 'pending':
            case 'waiting-for-acceptance':
                $status = 'pending';
                break;
            case 'in-route':
            case 'in-shop':
                $status = 'in-route';
                break;
            case 'accepted':
                $status = 'accepted';
                break;
            case 'delivered':
                $status = 'delivered';
                break;
            default:
                $status = 'canceled';

        }
        return $status;
    }

}
