<?php


namespace App\Domains\Order\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
      $details = $this->buy;
      $response = [
        'id' => $this->id,
        'type' => $this->buy_type,
        'name' => $details->name,
        'price' => $this->price,
        'quantity'=>$this->quantity,
        'image' => is_null($details->image) ? "" : $details->image,
        'brand' => $this->buy_type == "Product" ?$details->brand->name : "",
        ];

        if(isset($this->item_details) && $this->item_details->count())
        {
          $response['additional_items'] = AdditionalAndVariantsItemsResource::collection($this->item_details)->groupBy('buy_type');
        }
	   return $response;
    }
}
