<?php


namespace App\Domains\Order\Http\Controllers;

use App\Domains\Order\Http\Requests\ChangeOrderDriverStatusRequest;
use App\Domains\Order\Http\Requests\DriverAcceptOrderRequest;
use App\Domains\Order\Http\Requests\DriverOrderListRequest;
use App\Domains\Order\Http\Resources\OrderHistoryResource;
use App\Facades\Order;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DriverOrderController extends Controller
{
    /**
     * @param DriverAcceptOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function AcceptOrder(DriverAcceptOrderRequest  $request)
    {
        try {

            $response = Order::DriverAcceptOrder($request->order_id,$request->status);;

        }catch(\Exception $e)
        {
            return response()->json(['message'=>$e->getMessage()],500);
        }

        return response()->json(['message' => $response]);
    }

    /**
     * @param DriverOrderListRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function DriverOrderList(DriverOrderListRequest  $request)
    {
        try {

            $orders = Order::DriverOrdersList($request->all());

        }catch(\Exception $e)
        {
            return response()->json(['message'=>$e->getMessage()],500);
        }

        return OrderHistoryResource::collection($orders);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ChangeOrderStatus(ChangeOrderDriverStatusRequest  $request)
    {
        try {
            $response = Order::UpdateOrderStatus($request->order_id,$request->status);
        }catch(\Exception $e)
        {
            return response()->json(['message'=>$e->getMessage()],500);
        }

        return response()->json(['message' => $response]);
    }

    /**
     * @param Request $request
     * @return OrderHistoryResource
     */
    public function getOrderById(Request $request)
    {
        $request->validate([
            'order_id' => ['required'] ,
        ]);

        try {
            $order = Order::getOrderById($request->order_id);

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()] ,500);
        }

        return new OrderHistoryResource($order);
    }
}
