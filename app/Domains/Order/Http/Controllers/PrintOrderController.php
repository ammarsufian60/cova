<?php


namespace App\Domains\Order\Http\Controllers;

use App\Domains\Branch\Models\Branch;
use App\Domains\Order\Models\Order;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PrintOrderController extends Controller
{

    public function PrintOrderInvoice(Request $request,$order_id)
    {
        $order = Order::find($order_id);

        return view('Invoice.order',compact('order'));


    }
}
