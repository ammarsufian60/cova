<?php


namespace App\Domains\Order\Http\Controllers;


use App\Domains\Order\Http\Requests\CreateOrderRequest;
use App\Domains\Order\Http\Requests\ProviderOrdersRequest;
use App\Domains\Order\Http\Resources\OrderHistoryResource;
use App\Domains\Order\Http\Resources\ProviderOrderDetailsResource;
use App\Domains\Order\Http\Resources\ProviderOrderHistory;
use App\Domains\User\Actions\SendSMSMessage;
use App\Events\PushPendingOrder;
use App\Facades\Address;
use App\Facades\Cart;
use App\Facades\Order;
use App\Facades\Shipment;
use App\Facades\Transaction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Events\PushPendingOrder;


class OrderController extends Controller
{
    public function create(CreateOrderRequest $request)
    {
        try {
            if (Auth::user()->points < 0 && $request->payment_method == 'CASH') {
                throw new \Exception('Please pay the points from recents orders');
            }
            $status = !is_null(Order::lastestOrder()) ? Order::lastestOrder()->status : null;
            if ($status == 'pending') {
                throw new \Exception('Please complete the Last order');
            }

            if (is_null(json_decode($request->items)) || is_null(json_decode($request->address))) {
                throw new \Exception('Something Wrong either items object or address');
            }

            //Create Cart
            $cart = Cart::create();

            // Attach Cart Item with cart
            $status = Cart::createCartItem($request->items, $cart->id);

            //Create Address For Order
            $address = Address::store((array)json_decode($request->address));

            //Create Order By Cart id && Address Id Locations
            $order = Order::create($cart->id, $address->id, $request->branch_id, $request->total);

            //Create Transaction
            $transaction = Transaction::create($order->id, $request->payment_method, $request->total);

            //Create Order Item Action
            Order::createOrderItem($order->id, $cart->id);

            //Create Shipment For Order
            Shipment::create($order->id, $request->branch_id, $request->shipment_price);

            (new SendSMSMessage('966540004718'))->handle();

             event(new PushPendingOrder($order,$request->branch_id));
        } catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage() ], 500);
        }

        return response()->json([ 'message' => 'successful' ]);

    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $status = null;
        if ($request->has('status')) {
            $status = $request->status;
        }
        $orders = Order::index($status);

        return OrderHistoryResource::collection($orders);
    }

    /**
     * @return OrderHistoryResource
     */
    public function lastOrder()
    {
        try {

            $orders = Order::lastestOrder();
            if (is_null($orders)) {
                throw new \Exception('false');
            }
        } catch (\Exception $e) {
            return response()->json([ 'message' => 'false' ], 500);
        }
        return new  OrderHistoryResource($orders);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function CheckUserRateOnLastOrder()
    {
        try {

            $order = Order::lastestOrder();
            if (is_null($order)) {
                throw new \Exception('false');
            }

            $rating = $order->ratings;

            if ($rating->count())
                return response()->json([ 'message' => 'false' ], 200);
            else if ($order->status == 'delivered') {
                return new OrderHistoryResource($order);
            } else {
                return response()->json([ 'message' => 'false' ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([ 'message' => 'false' ], 500);
        }

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function CheckUserPaidOnLastOrder()
    {
        try {
            $orders = Order::lastestOrder();

            if (is_null($orders)) {
                throw new \Exception('false');
            }

            $status = $orders->status;
        } catch (\Exception $e) {
            return response()->json([ 'message' => 'false' ], 500);
        }
        return response()->json([ 'message' => !($status === 'return-to-shipper') ], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function CheckStatusOnLastOrder()
    {
        try {
            $orders = Order::lastestOrder();

            if (is_null($orders)) {
                throw new \Exception('false');
            }
            $status = $orders->status;

            switch ($status) {
                case 'pending':
                case 'waiting-for-acceptance':
                case 'in-route':
                case 'in-shop':
                case 'accepted':
                    $status = 'true';
                    break;
                default:
                    $status = 'false';
            }
        } catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage() ], 500);
        }

        return response()->json([ 'message' => $status ], 200);
    }

    /**
     * @param ProviderOrdersRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getProviderBranchOrder(ProviderOrdersRequest $request)
    {
        try {
            $orders = Order::filterBranchOrders($request->branch_id, $request->status, $request->from_date, $request->to_date);
        } catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage() ], 500);
        }

        return ProviderOrderHistory::collection($orders);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setOrderStatusInRoute(Request $request)
    {
        $request->validate([
            'order_id' => [ 'required' ]
        ]);

        try {
            $response = Order::setOrderStatusInRoute($request->order_id);

        } catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage() ], 500);
        }

        return response()->json([ 'message' => 'success' ]);
    }

    /**
     * @param $id
     * @return ProviderOrderDetailsResource|\Illuminate\Http\JsonResponse
     */
    public function getOrderDetailsById($id)
    {
        try {
            $order = Order::getOrderById($id);

        } catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage() ], 500);
        }
        return new ProviderOrderDetailsResource($order);
    }
}
