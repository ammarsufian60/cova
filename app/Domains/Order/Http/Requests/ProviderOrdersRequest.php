<?php


namespace App\Domains\Order\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ProviderOrdersRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return[
           'from_date' => ['nullable'],
           'to_date' => ['nullable'],
           'status'  => ['required'],
        ];
    }

}
