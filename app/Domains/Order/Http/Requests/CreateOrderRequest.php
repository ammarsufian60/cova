<?php


namespace App\Domains\Order\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return[
            'address' => ['required'],
            'items' => ['required'],
            'payment_method' => ['required','string'],
            'payment_id' => ['required_if:payment_method,ONLINE'],
            'total' => ['required'],
            'branch_id'=>['required','exists:branches,id'],
        ];
    }

}
