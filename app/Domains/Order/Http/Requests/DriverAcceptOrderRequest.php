<?php


namespace App\Domains\Order\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class DriverAcceptOrderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return[
            'order_id' =>['required'],
        ];
    }

}
