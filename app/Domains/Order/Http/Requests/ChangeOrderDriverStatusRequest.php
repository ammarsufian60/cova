<?php


namespace App\Domains\Order\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class ChangeOrderDriverStatusRequest  extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return[
            'order_id' =>['required','exists:orders,id'],
            'status' =>['required','in:out-for-delivery,in-route,in-shop,delivered,returned-to-shipper']
        ];
    }

}
