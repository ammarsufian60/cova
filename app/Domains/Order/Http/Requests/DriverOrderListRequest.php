<?php


namespace App\Domains\Order\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class DriverOrderListRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return[
            'latitude' =>['required'],
            'longitude' => ['required'],
        ];
    }

}
