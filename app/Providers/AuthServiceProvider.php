<?php

namespace App\Providers;

use App\Domains\Address\Models\Address;
use App\Domains\Branch\Models\Branch;
use App\Domains\Brand\Models\Brand;
use App\Domains\Cart\Models\Cart;
use App\Domains\Cart\Models\CartItem;
use App\Domains\Category\Models\Category;
use App\Domains\ContactUs\Models\ContactUS;
use App\Domains\Order\Models\Order;
use App\Domains\Order\Models\OrderItem;
use App\Domains\Product\Models\AdditionalItem;
use App\Domains\Product\Models\Product;
use App\Domains\Product\Models\ProductVariant;
use App\Domains\Rating\Models\Rating;
use App\Domains\Service\Models\Service;
use App\Domains\Shipment\Models\Shipment;
use App\Domains\User\Models\Driver;
use App\Domains\User\Models\Permission;
use App\Domains\User\Models\Role;
use App\Policies\AdditionItemPolicy;
use App\Policies\AddressPolicy;
use App\Policies\BranchPolicy;
use App\Policies\BrandPolicy;
use App\Policies\CartItemPolicy;
use App\Policies\CartPolicy;
use App\Policies\CategoryPolicy;
use App\Policies\ContactPolicy;
use App\Policies\DriverPolicy;
use App\Policies\OrderItemPolicy;
use App\Policies\OrderPolicy;
use App\Policies\PermissionPolicy;
use App\Policies\ProductPolicy;
use App\Policies\ProductVariantPolicy;
use App\Policies\RatingPolicy;
use App\Policies\RolePolicy;
use App\Policies\ServicePolicy;
use App\Policies\ShipmentPolicy;
use App\Policies\UserPolicy;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Brand::class => BrandPolicy::class,
        Cart::class => CartPolicy::class,
        Order::class => OrderPolicy::class,
        CartItem::class => CartItemPolicy::class,
        OrderItem::class => OrderItemPolicy::class,
        Permission::class => PermissionPolicy::class,
        Role::class => RolePolicy::class,
        Service::class => ServicePolicy::class,
        Category::class => CategoryPolicy::class,
        Branch::class => BranchPolicy::class,
        Address::class => AddressPolicy::class,
//        Driver::class => DriverPolicy::class,
        Product::class => ProductPolicy::class,
        ProductVariant::class => ProductVariantPolicy::class,
        AdditionalItem::class => AdditionItemPolicy::class,
//        Rating::class => RatingPolicy::class,
        ContactUS::class => ContactPolicy::class,
        Shipment::class => ShipmentPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
