<?php

namespace App\Providers;

use App\Domains\Product\Models\AdditionalItem;
use App\Domains\Product\Models\Product;
use App\Domains\Product\Models\ProductVariant;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'Product' => Product::class,
            'ProductVariant' => ProductVariant::class,
            'AdditionalItem' => AdditionalItem::class,
            ]);
    }
}
