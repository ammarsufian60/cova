<?php


namespace App\Facades;


use App\Domains\Category\CategoryServices;
use Illuminate\Support\Facades\Facade;

class Category extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return CategoryServices::class;
    }

}
