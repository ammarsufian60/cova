<?php


namespace App\Facades;


use App\Domains\Rating\RatingService;
use Illuminate\Support\Facades\Facade;

class Rating extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return RatingService::class ;
    }

}
