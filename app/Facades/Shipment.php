<?php


namespace App\Facades;


use App\Domains\Shipment\ShipmentServices;
use Illuminate\Support\Facades\Facade;

class Shipment extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return ShipmentServices::class ;
    }
}
