<?php


namespace App\Facades;


use App\Domains\Branch\BranchServices;
use Illuminate\Support\Facades\Facade;

class BranchFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return BranchServices::class ;
    }

}
