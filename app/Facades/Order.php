<?php


namespace App\Facades;


use App\Domains\Order\OrderServices;
use Illuminate\Support\Facades\Facade;

class Order extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
      return OrderServices::class;
    }

}
