<?php


namespace App\Facades;


use App\Domains\Address\AddressServices;
use Illuminate\Support\Facades\Facade;

class Address extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return AddressServices::class;
    }

}
