<?php

namespace App\Nova;

use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use \Laravel\Nova\Fields\BelongsTo;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Panel;

class Branch extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Domains\Branch\Models\Branch::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {

        return [
            ID::make()->sortable(),
            Text::make('Name')
                ->help('Branch name required'),
            Text::make('Latitude')
                 ->help('Branch latitude required'),
            Text::make('Longitude')
                ->help('Branch longitude required'),
            Text::make('Open Hour','open_hour')
                ->help('Branch Open Hour required'),
            Text::make('Close Hour','close_hour')
                ->help('Branch Close Hour required'),
            Boolean::make('Status'),

            BelongsTo::make('Brand','brand',Brand::class),

            BelongsTo::make('Manager','manager',Manager::class),

            Images::make('Image','branch')->help('Branch images must be have 227 * 123'),
            BelongsToMany::make('Categories','categories',Category::class),
            Text::make('Estimate Delivery Time','estimate_delivery_time'),
            Boolean::make('Discounted Branch','is_discounted')->rules(['required']),
            Number::make('Discount Rate','discount_rate')->rules(['required']),
            HasMany::make('Rating','ratings',Rating::class),
            (new Panel('Branch Contract',$this->getBranchContract())),
        ];
    }

    public function getBranchContract()
    {
        return [
            Select::make('Subscription Type','subscription_type')->options([
                'subscription' =>'subscription',
                'percentage-on-sales' => 'percentage-on-sales'
            ]),

            Number::make('Subscription Amount','subscription_amount'),

            Images::make('Contract Image 1','contractFront')
                ->rules(['required'])->help('Brand images must be have 227 * 123'),

            Images::make('Contract Image 2','contractBack')
                ->rules(['required'])->help('Brand images must be have 227 * 123'),

            DateTime::make('Contract Expire At','contract_expiry_date')
        ];
    }
    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
