<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Provider extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\User::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make('ID'),
            Text::make('Name')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Mobile Number', 'mobile_number')
                ->sortable()
                ->rules('required', 'max:12'),

            Text::make('Email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            Password::make('Password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8'),

            Select::make('Role')->options([
                'provider' => 'provider'
            ]),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
        ];
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizedToView(Request $request)
    {
        return Auth::user()->hasRole('super-admin') || Auth::user()->hasPermissionTo('view-provider');
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizedToUpdate(Request $request)
    {
        return Auth::user()->hasRole('super-admin') || Auth::user()->hasPermissionTo('update-provider');
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizeToViewAny(Request $request)
    {
        return Auth::user()->hasRole('super-admin') || Auth::user()->hasPermission('view-provider');
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizeToView(Request $request)
    {
        return Auth::user()->hasRole('super-admin') || Auth::user()->hasPermission('view-provider');
    }

    /**
     * @param Request $request
     * @return bool
     */
    public static function authorizedToCreate(Request $request)
    {
        return Auth::user()->hasRole('super-admin') ||Auth::user()->hasPermissionTo('create-provider');
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->where('role','provider');
    }
}
