<?php

namespace App\Nova;

use App\Domains\Account\Actions\ExportAccounts;
use App\Domains\Account\Models\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\BelongsTo;

class Accounts extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = Account::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'branch.name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     *
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Order Total','order_total'),
            Text::make('Delivery Fees','delivery_orders_fees'),
            Text::make('Credit Card Order Fees','credit_card_order_fees'),
            Text::make('Total Fees','total_fees'),
            Text::make( 'Payment Total','payment_total'),
            Boolean::make('is_paid'),
            BelongsTo::make('Branch','branch',Branch::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new ExportAccounts())->withHeadings('branch name', 'Order Total', 'Total Delivery Fees','Total of Credit card Fees','Total Fees','Payment Total', 'Paid'),
        ];
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizedToView(Request $request)
    {
        return Auth::user()->hasRole('super-admin');
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizedToUpdate(Request $request)
    {
        return false;
    }

    /**
     * @param Request $request
     * @return false|void
     */
    public static function authorizeToCreate(Request $request)
    {
        return false;
    }
}
