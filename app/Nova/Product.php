<?php

namespace App\Nova;

use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Product extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Domains\Product\Models\Product::class;

    public static $group ='Product care';
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Name'),
            Text::make('Description'),
            Number::make('Price')->step(0.1),
            Number::make('Quantity')->rules('max:4')->help('max value can be added = 1000'),
            Boolean::make('Is Available','is_available'),
            Boolean::make('Is Addition Item','is_additional_item'),
            BelongsTo::make('Category','category',Category::class),
            BelongsTo::make('Brand','brand',Brand::class),
            BelongsToMany::make('Variants','variants',ProductVariant::class)->fields(function () {
                return [
                    Text::make('Price', 'price')->showOnIndex(),
                ];
            }),
            BelongsToMany::make('Additional Items','additional_items',AdditionalItem::class)->fields(function(){
                 return [
                    Text::make('Price', 'price')->showOnIndex(),
                   ];
            }),
            Images::make('Image','product')
//                ->creationRules('required')
//		 ->rules('required')
                ->help('Brand images must be have 227 * 123'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
