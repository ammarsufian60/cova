<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Fields\Select;

class ChangeOrderStatus extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        collect($models)->each(function($order)use($fields){
            $status = $fields['status'];

            if($status == 'returned-to-shipper'){
                $current_points = Auth::user()->points;
                Auth::user()->update(['points' => $current_points - $order->transaction->amount]);
            }

             $order->update(['status' => $status ]) ;
        });
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Select::make('Status','status')->options([
                'pending'  => 'Pending',
                'accepted' => 'Accept Order',
                'rejected' => 'Reject Order',
                'returned-to-shipper' => 'Returned To Shipper',
                'in-route' => 'In Route',
                'in-shop' => 'In Shop',
                'delivered'=>'delivered'
             ])
        ];
    }
}
