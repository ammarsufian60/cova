<?php

namespace App\Nova\Actions;

//use App\User;
use App\Nova\Driver;
use App\Nova\DriverUser;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Sloveniangooner\SearchableSelect\SearchableSelect;

class AssignOrderToDriver extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        collect($models)->each(function($order) use($fields) {
           $order->shipments->first()->update(['driver_id'=> $fields['driver_id']]) ;

//           User::find($fields['driver_id'])->
        });
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
    	  SearchableSelect::make("Drivers", "driver_id")->resource(DriverUser::class)
       ];
    }
}
