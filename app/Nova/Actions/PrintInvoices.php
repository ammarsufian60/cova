<?php

namespace App\Nova\Actions;

use App\Domains\Order\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class PrintInvoices extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $ids =$models->pluck('id');

        $orders = Order::whereIn('id',$ids)->get();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('Invoice.order', compact('orders'));
        $id = uniqid();
        $path = '/storage/'. $id .'.pdf';
        $pdf->save(public_path($path));

        return Action::download(url($path), uniqid() . '.pdf');
//
//        $ids =$models->pluck('id');
//
//        $order = Order::find($ids[0]);
//
//        return Action::openInNewTab(route("Invoice", ['order' => $order]));

    }


}
