<?php

namespace App\Nova;

use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Password;

class Driver extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\User::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Text::make('Name','name'),
            Number::make('Mobile Number', 'mobile_number'),
            Text::make('Email','email')->rules(['required','email']),
            Password::make('Password','password')->showOnIndex(false)->showOnDetail(false),
            Number::make('Accept shipments count','accepted_shipments')->onlyOnIndex(),
            Number::make('Rejected shipments count','rejected_shipments')->onlyOnIndex(),
            Number::make('Completed shipments count','completed_shipments')->onlyOnIndex(),
            Text::make('Status'),
            HasMany::make('Shipment','shipments',Shipment::class),
            Select::make('Role','role')->options([
               'driver'=>'driver'
            ]),

            Images::make('Profile Image','driver'),

            Text::make('Company Name','company_name'),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->whereHas('roles',function($query){
            $query->where('name','driver');
        })->get();
    }

     /**
     * @param Request $request
     * @return bool
     */
    public function authorizedToView(Request $request)
    {
        return true;
//      return Auth::user()->hasRole('super-admin') || Auth::user()->hasRole('driver-management') || Auth::user()->hasPermission('view-driver');
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizedToUpdate(Request $request)
    {
        return Auth::user()->hasRole('super-admin') || Auth::user()->hasRole('driver-management') || Auth::user()->hasPermission('update-driver');
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizeToViewAny(Request $request)
    {
       return true;
 //       return Auth::user()->hasRole('super-admin') || Auth::user()->hasRole('driver-management') || Auth::user()->hasPermission('view-driver');
    }

    /**
     * @param Request $request
     * @return bool
     */
    public static function authorizedToCreate(Request $request)
    {
      return true;
     // return Auth::user()->hasRole('super-admin') || Auth::user()->hasRole('driver-management') ;
    }
}
