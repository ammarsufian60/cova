<?php

namespace App\Nova;

use Laravel\Nova\Fields\Number;
use Illuminate\Support\Facades\Auth;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\Password;
use Vyuldashev\NovaPermission\Permission;
use Vyuldashev\NovaPermission\Role;
use App\Nova\Actions\AssignRole;
use Laravel\Nova\Fields\Select;
//use App\Domains\User\Models\Role ;

class User extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\\User';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name', 'email','mobile_number'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Name')
                ->sortable()
                ->rules('required', 'max:255'),

            Number::make('Mobile Number','mobile_number')
                ->sortable()
                ->creationRules('required', 'max:12'),

            Text::make('Email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            Password::make('Password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8'),

           Select::make('Role')->options([
            'super-admin' =>'Super Admin',
            'driver-management' => 'Driver Management',
            'support' => 'Support' ,
	    'manager' => 'Manager',   
            ]),

           Text::make('Points')->readOnly(),


           Text::make('Company Name', 'company_name')
               ->help('Please Fill it Only when Create Driver management account')->nullable(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
    	  (new AssignRole())
        ];
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizedToView(Request $request)
    {
        return Auth::user()->hasRole('super-admin');
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizedToUpdate(Request $request)
    {
        return Auth::user()->hasRole('super-admin');
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function authorizeToViewAny(Request $request)
    {
         return Auth::user()->hasRole('super-admin') ||  Auth::user()->hasPermission('view-user');
    }
    /**
     * @param Request $request
     * @return bool
     */
    public function authorizeToView(Request $request)
    {
         return Auth::user()->hasRole('super-admin') ||  Auth::user()->hasPermission('view-user');
    }
    /**
     * @param Request $request
     * @return bool
     */
    public static function authorizedToCreate(Request $request)
    {
        return Auth::user()->hasRole('super-admin') ; //|| Auth::user()->hasRole('driver-management') ;
    }
}
