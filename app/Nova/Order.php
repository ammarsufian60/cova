<?php

namespace App\Nova;

use App\Domains\Order\Models\Order as orders;
use App\Nova\Actions\PrintInvoices;
use App\Nova\Filters\FilterOrderByBranch;
use Otrsw\LeafletMap\LeafletMap;
use App\Nova\Actions\AssignOrderToDriver;
use App\Nova\Actions\ChangeOrderStatus;
use Cova\UsersInfo\UsersInfo;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Carlson\NovaLinkField\Link;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Panel;

class Order extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */

    public static $model = \App\Domains\Order\Models\Order::class;

    public static $group = 'Order Care';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
 
            Link::make('Invoice', 'id')->details([ 'href' => "https://cova-app.com/invoice/".$this->id,
              'text' => 'Print Invoice', 'newTab' => true
            ]), 
            //ID::make()->sortable(),
            Text::make('Status'),
            BelongsTo::make('User','user',User::class),
            HasMany::make('Order Items','items',OrderItem::class),
            HasOne::make('Transaction','transaction',Transaction::class),
            HasMany::make('Shipment','shipments',Shipment::class),
            HasMany::make('Rating','ratings',Rating::class),
            Text::make('Amount',function(){return $this->transaction->amount;}),
            Text::make('Payment Method',function(){return $this->transaction->payment_method;}),
            Text::make('OrderItems',function(){
               return $this->cart->items->pluck('buy.name');
            }),
            HasMany::make('Rating','ratings',Rating::class),
            BelongsTo::make('Branch','branch',Branch::class),
            Text::make('Address',function(){
                return $this->address->city.'-'.$this->address->area;
             }),
            DateTime::make('Created At ','created_at'),

            LeafletMap::make('Current Location of Customer')
                ->type('LatLon')
                ->point($this->my_latitude(),$this->my_longitude())
	        ->zoom(12)
                ->onlyOnDetail(),

            Link::make('Location', 'id')->details([
                'href' => "https://cova-app.com/maps/".$this->id,
                'text' => 'Customer Location from Google maps',
                'newTab' => true
            ])->onlyOnDetail(),

       ];
    }

    /** * Get the cards available for the request. * * @param \Illuminate\Http\Request $request * @return array */
    public function cards(Request $request)
    {
        return [
           ( new UsersInfo(request()->resourceId))->user()->onlyOnDetail(),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new FilterOrderByBranch()
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
          (new ChangeOrderStatus())->canRun(function(NovaRequest $request) {
                return true;
            }),
          (new AssignOrderToDriver())->canRun(function(NovaRequest $request) {
		      return true;
            }),
            /*(new PrintInvoices())->canRun(function(NovaRequest $request) {
                return true;
            }),*/
        ];
    }

    /**
     * @return int
     */
    public function my_latitude()
    {

         return (float) !is_null($this->address)?$this->address->latitude : null;
    }

    /**
     * @return int
     */
    public function my_longitude()
    {

         return (float) !is_null($this->address)?$this->Address->longitude :null;

    }
}
