<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class PendingOrders extends Resource
{


    /**
     * The model the resource corresponds to.
     *
     * @var string
     */

    public static $model = \App\Domains\Order\Models\Order::class; 
        public static $group = 'Order Care';

	
    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            Text::make('Status'),
            BelongsTo::make('User','user',User::class),
            HasMany::make('Order Items','items',OrderItem::class),
            HasOne::make('Transaction','transaction',Transaction::class),
            HasMany::make('Shipment','shipments',Shipment::class),
            HasMany::make('Rating','ratings',Rating::class),
            Text::make('Amount',function(){return $this->transaction->amount;}),
            Text::make('OrderItems',function(){
                return $this->cart->items->pluck('buy.name');
                // return $items;
            }),
            HasMany::make('Rating','ratings',Rating::class),
            BelongsTo::make('Branch','branch',Branch::class),
            Text::make('Address',function(){
                return $this->address->city.'-'.$this->address->area;
            }),
            DateTime::make('Created At ','created_at'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->where('status','pending')->get();
    }
}
