<?php

namespace App\Nova\Filters;

use App\Domains\Brand\Models\Brand;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class FilterOrderByBranch extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
     public $component = 'select-filter';

     public $name ="Filter By brand name ";
    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        if (!is_null($value)) {
            $branches_id = Brand::find($value)->branches->pluck('id');
            return $query->whereIn('branch_id', $branches_id)->get();
        }
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
          $branches =  Brand::pluck('id','name');

         return $branches;


    }
}
