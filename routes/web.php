<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});




Route::get('/invoice/{order_id}','Order\Http\Controllers\PrintOrderController@PrintOrderInvoice');
Route::get('/maps/{order_id}','Branch\Http\Controllers\MapsController');
