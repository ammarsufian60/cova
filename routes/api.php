<?php

use Illuminate\Http\Request;




/**
 * Authorization Requests
 */

Route::post('/login','User\Http\Controllers\AuthController@login');
Route::post('/register','User\Http\Controllers\AuthController@register');
Route::post('/forget-password','User\Http\Controllers\AuthController@forgetPassword');
Route::middleware('auth:api')->post('/change-password','User\Http\Controllers\UserController@changePassword');
Route::middleware('auth:api')->get('profile','User\Http\Controllers\UserController@userProfile');
Route::middleware('auth:api')->put('update-profile','User\Http\Controllers\UserController@updateProfile');


/**
 * OTP Requests
 */

Route::post('/otp','User\Http\Controllers\OtpVerificationController@sendOtp');
Route::middleware('auth:api')->get('/otp-verify','User\Http\Controllers\OtpVerificationController@verify');

/**
 *  Branches Location
 */
Route::get('/locations','Branch\Http\Controllers\BranchController@nearby');
Route::get('/discounted_locations','Branch\Http\Controllers\BranchController@discounted');

/**
 * Address Requests
 */
Route::middleware('auth:api')->get('/addresses','Address\Http\Controllers\AddressController@index');
Route::middleware('auth:api')->post('/address','Address\Http\Controllers\AddressController@store');


/**
 * Services Requests
 */

Route::get('/service','Service\Http\Controllers\ServiceController');


/**
 * Ratings Requests
 */

Route::middleware('auth:api')->post('/rating','Rating\Http\Controllers\RatingController@store');

/**
 * Categories Requests
 */

Route::get('/categories','Category\Http\Controllers\CategoriesController@show');

/**
 * Products Requests
 */

Route::get('/products','Product\Http\Controllers\ProductsController@index');

/**
 * Order Requests
 */

Route::middleware('auth:api')->post('/order','Order\Http\Controllers\OrderController@create');
Route::middleware('auth:api')->get('/order','Order\Http\Controllers\OrderController@index');
Route::middleware('auth:api')->get('/latest-order','Order\Http\Controllers\OrderController@lastOrder');
Route::middleware('auth:api')->get('/check-last-order-rating','Order\Http\Controllers\OrderController@CheckUserRateOnLastOrder');
Route::middleware('auth:api')->get('/check-last-order-payment','Order\Http\Controllers\OrderController@CheckUserPaidOnLastOrder');
Route::middleware('auth:api')->get('/check-last-order-status','Order\Http\Controllers\OrderController@CheckStatusOnLastOrder');

/**
 * contact us Requests
 */
Route::post('/contact','ContactUs\Http\Controllers\ContactController@store');

/**
 * Shipment Price Requests
 */
Route::post('/shipment-price','Shipment\Http\Controller\ShipmentController@calculate');

/**
 * App Provider apis
 */
Route::prefix('provider')->group(function(){

    Route::middleware(['provider_only_public'])->post('/login','User\Http\Controllers\AuthController@login');

    Route::middleware(['auth:api', 'provider_only'])->post('/branch','Branch\Http\Controllers\BranchController@getBranchByManager');

    Route::middleware(['auth:api', 'provider_only'])->post('/orders','Order\Http\Controllers\OrderController@getProviderBranchOrder');

    Route::middleware(['auth:api', 'provider_only'])->get('/order/{id}','Order\Http\Controllers\OrderController@getOrderDetailsById');

    Route::middleware(['auth:api', 'provider_only'])->get('/profile','User\Http\Controllers\ProviderController@profile');

    Route::middleware(['provider_only_public'])->post('/forget-password','User\Http\Controllers\AuthController@forgetPassword');

    Route::middleware(['provider_only_public'])->post('/otp','User\Http\Controllers\OtpVerificationController@sendOtp');

    Route::middleware(['auth:api', 'provider_only'])->get('/otp-verify','User\Http\Controllers\OtpVerificationController@verify');

    Route::middleware(['auth:api', 'provider_only'])->post('/set-order-in-route','Order\Http\Controllers\OrderController@setOrderStatusInRoute');

});

/**
 * App Driver apis
 */
Route::prefix('driver')->group(function(){

    Route::post('/register','User\Http\Controllers\DriverController@register');

    Route::post('/login','User\Http\Controllers\DriverController@login');

    Route::middleware(['auth:api', 'driver_only'])->post('/accept-order','Order\Http\Controllers\DriverOrderController@AcceptOrder');

    Route::middleware(['auth:api','driver_only'])->post('/orders','Order\Http\Controllers\DriverOrderController@DriverOrderList');

    Route::middleware(['auth:api', 'driver_only'])->post('/change-driver-status','User\Http\Controllers\DriverController@updateDriverStatus');

    Route::middleware(['auth:api', 'driver_only'])->post('/change-shipment-status','Order\Http\Controllers\DriverOrderController@ChangeOrderStatus');

    Route::middleware(['auth:api', 'driver_only'])->post('/bank-details','User\Http\Controllers\DriverController@addBankDetails');

    Route::middleware(['auth:api', 'driver_only'])->post('/order','Order\Http\Controllers\DriverOrderController@getOrderById');

    Route::middleware(['auth:api', 'driver_only'])->get('/profile','User\Http\Controllers\DriverController@DriverProfile');

});
