<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('order_total',7,1);
            $table->decimal('delivery_orders_fees',7,1);
            $table->decimal('credit_card_order_fees',7,2);
            $table->decimal('total_fees',7,2);
            $table->decimal('payment_total',7,2);
            $table->tinyInteger('is_paid')->default(0);

            /**
             * Branch Id foreign key
             */

            $table->unsignedBigInteger('branch_id');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
