<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserTableSeeder extends Seeder
{
    protected $roles;

    protected $actions;

    protected $resources;

    public function __construct()
    {
        $this->roles = collect([
            'super-admin', 'merchant', 'support','driver'
        ]);

        $this->actions = collect(['view', 'create', 'update', 'delete', 'restore', 'force-delete']);

        $this->resources = collect([
            'address', 'brand', 'cart',
            'user','cart-item', 'category', 'order',
            'order-item', 'product','branch','transaction',
            'product-variant', 'addition-item','driver','contact','shipment','provider',
        ]);
    }

    public function run()
    {
        $this->createRoles();
        $this->createPermissions();
        $this->attachPermissionsToRoles();
    }

    protected function createRoles()
    {
        $this->roles->each(function($role){
            Role::firstOrCreate([
                'name' => $role
            ]);
        });
    }

    protected function createPermissions()
    {
        $this->resources->each(function($resource) {
            $this->actions->each(function($action) use ($resource) {
                Permission::firstOrCreate(['name' => $this->getPermissionName($action, $resource)]);
            });
        });
    }

    protected function attachPermissionsToRoles()
    {
        Role::where('name', 'support')
            ->first()
            ->syncPermissions([
                'view-brand',
                'view-branch',
                'view-cart',
                'view-cart-item',
                'view-category',
                'view-order',
                'view-order-item',
                'view-product',
                'view-user',
                'view-transaction',

            ]);

    }

    protected function getPermissionName($action, $resource)
    {
        return "$action-$resource";
    }
}
