<?php

namespace Cova\UsersInfo;

use App\User;
use Laravel\Nova\Card;
use App\Domains\Order\Models\Order;

class UsersInfo extends Card
{
    public function __construct($order_id)
    {

        parent::__construct();
        $order = Order::find($order_id) ;
 
       $this->user = !is_null($order)? $order->user: null ;
    }

    /**
     * The width of the card (1/3, 1/2, or full).
     *
     * @var string
     */
    public $width = '1/2';

    /**
     * Get the component name for the element.
     *
     * @return string
     */
   
    public function currentVisitors()
    {
        return $this->withMeta(['currentVisitors' => 9943]);
    }
   
    public function user()
    {
//        return $this->user;
	return $this->withMeta(['user' => $this->user]);  
  }

    public function component()
    {
        return 'users-info';
    }
}
